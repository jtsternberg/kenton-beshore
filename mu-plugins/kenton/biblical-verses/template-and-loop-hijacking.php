<?php
/**
 * Template and Loop Hijacking for Biblical Verses
 */

add_action( 'init', 'kb_biblical_verse_browse_pages_rewrites' );
/**
 * Add biblical verse rewrites.
 */
function kb_biblical_verse_browse_pages_rewrites() {
	global $wp_rewrite;
	$slug = 'browse-biblical-verses';

	add_rewrite_tag( '%view%', '([^&]+)' );
	add_rewrite_rule( "{$slug}/?$", 'index.php?taxonomy=biblical-verse&view=browse', 'top' );
	add_rewrite_rule( "{$slug}/(.*)/?$", 'index.php?taxonomy=biblical-verse&view=browse&term=$matches[1]', 'top' );
}

add_action( 'wp', 'kb_biblical_verse_browse_pages_setup_query', 9 );

/**
 * After WordPress queries for posts, override the query with dummy data
 * if a taxonomy term archive is expected.
 */
function kb_biblical_verse_browse_pages_setup_query() {
	$taxonomy = get_query_var( 'taxonomy' );
	$term_name = get_query_var( 'term' );
	$view = get_query_var( 'view' );
	// Bail early if we're not on a taxonomy term archive.
	if ( empty( $taxonomy ) || $view != 'browse' ) {
		return;
	}
	// Set a bool global so we can conditionally check we're on a taxonomy term archive later.
	set_is_biblical_verse_browse_page();

	kb_biblical_verse_browse_pages_stuff_wp_query_global_with_dummy_data();

	add_action( 'body_class', 'kb_biblical_verse_browse_pages_add_body_classes', 10, 1 );
}


function kb_biblical_verse_browse_pages_stuff_wp_query_global_with_dummy_data() {
	global $wp_query, $post;
	$taxonomy_name = get_query_var( 'taxonomy' );
	$taxonomy = get_taxonomy( $taxonomy_name );
	$term_slug = get_query_var( 'term' );
	if ( ! empty( $term_slug ) ) {
		$term = get_term_by( 'slug', $term_slug, 'biblical-verse' );
		$post_title = $term->name;
	} else {
		$post_title = $taxonomy->labels->name;
	}

	$dummy = array(
		'ID'                    => -9999,
		'post_status'           => 'public',
		'post_author'           => 0,
		'post_parent'           => 0,
		'post_type'             => 'page',
		'post_date'             => 0,
		'post_date_gmt'         => 0,
		'post_modified'         => 0,
		'post_modified_gmt'     => 0,
		'post_content'          => '',
		'post_title'            => $post_title,
		'post_excerpt'          => '',
		'post_content_filtered' => '',
		'post_mime_type'        => '',
		'post_password'         => '',
		'post_name'             => '',
		'guid'                  => '',
		'menu_order'            => 0,
		'pinged'                => '',
		'to_ping'               => '',
		'ping_status'           => '',
		'comment_status'        => 'closed',
		'comment_count'         => 0,
		'filter'                => 'raw',

		'is_404'                => false,
		'is_page'               => false,
		'is_single'             => false,
		'is_archive'            => false,
		'is_tax'                => false,
	);

	// Set the $post global
	$post = new WP_Post( (object) $dummy );

	// Copy the new post global into the main $wp_query
	$wp_query->post       = $post;
	$wp_query->posts      = array( $post );

	// Prevent comments form from appearing
	$wp_query->post_count = 1;
	$wp_query->is_404     = $dummy['is_404'];
	$wp_query->is_page    = $dummy['is_page'];
	$wp_query->is_single  = $dummy['is_single'];
	$wp_query->is_archive = $dummy['is_archive'];
	$wp_query->is_tax     = $dummy['is_tax'];
}

/**
 * Setter for the is_taxonomy_archive global.
 *
 * @global boolean $taxonomy_archive
 */
function set_is_biblical_verse_browse_page() {
	global $biblical_verse_browse_page;
	$biblical_verse_browse_page = true;
}

/**
 * Conditional tag for taxonomy terms archive.
 *
 * @global boolean $taxonomy_terms_archive
 *
 * @return boolean
 */
function is_biblical_verse_browse_page() {
	global $biblical_verse_browse_page;
	if ( ! empty( $biblical_verse_browse_page ) && $biblical_verse_browse_page ) {
		return true;
	} else {
		return false;
	}
}


add_filter( 'template_include', 'kb_biblical_verse_browse_page_template_include', 10, 1 );
/**
 * If the query matches a taxnomy terms archive, return the template path.
 *
 * First the template taxonomy-terms-{taxonomy-name}.php is given priority, followed by
 * taxonomy-terms.php.
 *
 * @since 0.1.0
 *
 * @param string $template Template path.
 *
 * @return string $template Template path.
 */
function kb_biblical_verse_browse_page_template_include( $template ) {
	if ( ! is_biblical_verse_browse_page() ) {
		return $template;
	}
	$term_name = get_query_var( 'term' );
	if ( empty( $term_name ) ) {
		if ( $template = locate_template( 'biblical-verse-browse-terms.php' ) ) {
			return $template;
		} else {
			_e( 'Could not find biblical-verse-browse-terms.php in your theme.' );
			die;
		}
	}
	if ( $template = locate_template( 'biblical-verse-browse-terms-' . $term_name . '.php' ) ) {
		return $template;
	} else if ( $template = locate_template( 'biblical-verse-browse-terms.php' ) ) {
		return $template;
	} else {
		_e( 'Could not find biblical-verse-browse-terms-' . $term_name . '.php in your theme.' );
		die;
	}
}


/**
 * Add useful classes to the body element on taxonomy terms archive.
 */
function kb_biblical_verse_browse_pages_add_body_classes( $classes ) {
	$classes[] = 'biblical-verse-browse-page';
	return $classes;
}