<?php

// require( 'template-and-loop-hijacking.php' );
// require( 'template-tags.php' );

add_action( 'init', 'kb_register_biblical_verse_taxonomy' );

/**
 * Register biblical verse taxonomy
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 * @return null|WP_Error WP_Error if errors, otherwise null.
 */
function kb_register_biblical_verse_taxonomy() {
		$labels = array(
			'name'					=> 'Biblical Verses',
			'singular_name'			=> 'Biblical Verse',
			'search_items'			=> 'Search Biblical Verses',
			'popular_items'			=> 'Popular Biblical Verses',
			'all_items'				=> 'All Biblical Verses',
			'parent_item'			=> 'Parent Biblical Verse',
			'parent_item_colon'		=> 'Parent Biblical Verse',
			'edit_item'				=> 'Edit Biblical Verse',
			'update_item'			=> 'Update Biblical Verse',
			'add_new_item'			=> 'Add New Biblical Verse',
			'new_item_name'			=> 'New Biblical Verse Name',
			'add_or_remove_items'	=> 'Add or remove Biblical Verse',
			'choose_from_most_used'	=> 'Choose from most used biblical verses',
			'menu_name'				=> 'Biblical Verses',
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => true,
			'show_tagcloud'     => true,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);
		register_taxonomy( 'biblical-verse', array( 'media' ), $args );
}

/**
 * Get verse links for the post in the loop.
 * @return array Links.
 */
function kb_get_biblical_verse_links() {
	// Get all terms related to the post
	$terms = wp_get_object_terms( get_the_ID(), 'biblical-verse' );

	// Extract top-level term IDs (old/new testaments)
	foreach ( $terms as $key => $term ) {
		if ( ! $term->parent ) {
			$top_level_parents[$term->term_id] = $term;
			unset( $terms[$key] ); // Won't be needing this again.
			continue;
		}
	}
	// Extract books
	foreach ( $terms as $key => $term ) {
		if ( ! in_array( (int) $term->parent, array_keys( $top_level_parents ) ) ) {
			continue;
		}
		$books[$term->term_id] = $term;
		unset( $terms[$key] ); // Won't be needing this again.
	}
	// Extract chapters
	foreach ( $terms as $term ) {
		if ( ! in_array( $term->parent, array_keys( $books ) ) ) {
			continue;
		}
		$books[$term->parent]->chapters[$term->term_id] = $term;
		unset( $terms[$key] ); // Won't be needing this again.
	}

	// Extract verses
	foreach ( $terms as $term ) {
		foreach ( $books as $key => $book ) {
			if ( ! in_array( $term->parent, array_keys( $book->chapters ) ) ) {
				continue;
			}
			$books[$key]->chapters[$term->parent]->verses[$term->term_id] = $term;
		}
	}

	foreach ( $books as $book_key => $book ) {
		foreach ( $book->chapters as $chapter_key => $chapter ) {
			foreach ( $chapter->verses as $verse_key => $verse ) {
				$verse_links[] = sprintf(
					'<a href="%s">%s %s:%s</a>',
					get_term_link( $verse, 'biblical-verse' ),
					$book->name,
					$chapter->name,
					$verse->name
				);
			}
		}
	}
	return $verse_links;
}
