<?php
/**
 * Whether biblical verses are related to the post in the loop.
 * @return bool
 */
function kb_has_biblical_verse() {
	return (bool) wp_get_object_terms( get_the_ID(), 'biblical-verse' );
}