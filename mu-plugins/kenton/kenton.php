<?php
/*
Plugin Name: Kenton Beshore Functionality
Description: Adds required functionality for the Kenton Beshore site.
Author: Eric Andrew Lewis
Version: 0.1
Author URI: http://www.ericandrewlewis.com
*/

// Taxonomy terms archives module.
// require( 'taxonomy-terms-archives/taxonomy-terms-archives.php' );

// Biblical Verses.
require( 'biblical-verses/biblical-verses.php' );

// require( 'search/index.php' );

require( 'topics/index.php' );

// add_action( 'init', 'kenton_add_media_series_taxonomy_terms_archive' );
/**
 * Add taxonomy terms archive support for the Media Series taxonomy.
 */
function kenton_add_media_series_taxonomy_terms_archive() {
	add_taxonomy_terms_archive( 'media-series', array( 'slug' => 'series-archive' ) );
}