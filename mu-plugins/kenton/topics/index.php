<?php

add_action( 'init', 'kb_register_topic_taxonomy' );

/**
 * Register topic taxonomy
 *
 */
function kb_register_topic_taxonomy() {
		$labels = array(
			'name'					=> 'Topics',
			'singular_name'			=> 'Topic',
			'search_items'			=> 'Search Topics',
			'popular_items'			=> 'Popular Topics',
			'all_items'				=> 'All Topics',
			'parent_item'			=> 'Parent Topic',
			'parent_item_colon'		=> 'Parent Topic',
			'edit_item'				=> 'Edit Topic',
			'update_item'			=> 'Update Topic',
			'add_new_item'			=> 'Add New Topic',
			'new_item_name'			=> 'New Topic Name',
			'add_or_remove_items'	=> 'Add or remove Topic',
			'choose_from_most_used'	=> 'Choose from most used Topics',
			'menu_name'				=> 'Topics',
		);

		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => false,
			'show_tagcloud'     => true,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);
		register_taxonomy( 'topic', array( 'media' ), $args );
}
