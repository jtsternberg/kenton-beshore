<?php
/**
 * CMB Theme Options
 * @version 0.1.0
 */
class KBeshore_Admin {

 	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = 'kenton_beshore_theme_settings';

	/**
	 * Array of metaboxes/fields
	 * @var array
	 */
	protected $option_metabox = array();

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'Settings', 'kbeshore' );
 	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
	}

	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
	}

	/**
	 * Admin page markup. Mostly handled by CMB
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		?>
		<div class="wrap cmb_options_page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb_metabox_form( self::option_fields(), $this->key ); ?>
		</div>
		<?php
	}

	/**
	 * Defines the theme option metabox and field configuration
	 * @since  0.1.0
	 * @return array
	 */
	public function option_fields() {

		// Only need to initiate the array once per page-load
		if ( ! empty( $this->option_metabox ) ) {
			return $this->option_metabox;
		}

		$this->fields = array(
			array(
				'name' => 'Contact Info',
				'id'   => 'contact_info',
				'type' => 'title',
			),
			array(
				'name'       => 'Phone #',
				'id'         => 'phone',
				'type'       => 'text_medium',
				'attributes' => array(
					'type' => 'number',
				),
			),
			array(
				'name' => 'Mailing Address',
				'id'   => 'address',
				'type' => 'textarea_small',
			),
			array(
				'name' => 'Twitter Handle',
				'id'   => 'twitter_handle',
				'type' => 'text_medium',
			),
			array(
				'name' => 'Facebook Link',
				'id'   => 'facebook_link',
				'type' => 'text_url',
			),

			array(
				'name' => 'Subscribe Links',
				'id'   => 'subscribe_links',
				'type' => 'title',
			),
			array(
				'name' => 'iTunes Subscribe Link',
				'id'   => 'subscribe_link_itunes',
				'type' => 'text_url',
			),
			array(
				'name' => 'Stitcher Subscribe Link',
				'id'   => 'subscribe_link_stitcher',
				'type' => 'text_url',
			),
			array(
				'name' => 'RSS Subscribe Link',
				'id'   => 'subscribe_link_rss',
				'type' => 'text_url',
			),
			array(
				'name' => 'Vimeo Subscribe Link',
				'id'   => 'subscribe_link_vimeo',
				'type' => 'text_url',
			),
			array(
				'name' => 'Youtube Subscribe Link',
				'id'   => 'subscribe_link_youtube',
				'type' => 'text_url',
			),

			array(
				'name' => 'Podcast Info',
				'id'   => 'podcast_info',
				'type' => 'title',
			),
			array(
				'name'    => 'Rate this podcast text',
				'id'      => 'rate_this_podcast_text',
				'type'    => 'wysiwyg',
				'options' => array(
					'textarea_rows' => 5,
					'tinymce'       => false,
				),
			),
			array(
				'name'    => 'Music for this podcast text',
				'id'      => 'music_for_this_podcast_text',
				'type'    => 'wysiwyg',
				'options' => array(
					'textarea_rows' => 5,
					'tinymce'       => false,
				),
			),

			array(
				'name' => 'Media Item Settings',
				'id'   => 'media_settings',
				'type' => 'title',
			),
			array(
				'name'    => 'Media Archive Header',
				'id'      => 'media_archive_header',
				'type'    => 'wysiwyg',
				'options' => array(
					'textarea_rows' => 5,
					'tinymce'       => false,
				),
			),
		);

		$this->option_metabox = array(
			'id'         => 'option_metabox',
			'show_on'    => array( 'key' => 'options-page', 'value' => array( $this->key, ), ),
			'show_names' => true,
			'fields'     => $this->fields,
		);

		return $this->option_metabox;
	}

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {

		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'fields', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		if ( 'option_metabox' === $field ) {
			return $this->option_fields();
		}

		throw new Exception( 'Invalid property: ' . $field );
	}

}

// Get it started
$KBeshore_Admin = new KBeshore_Admin();
$KBeshore_Admin->hooks();

/**
 * Wrapper function around cmb_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function kbeshore_get_option( $option_name = '' ) {
	global $KBeshore_Admin;
	return cmb_get_option( $KBeshore_Admin->key, $option_name );
}
