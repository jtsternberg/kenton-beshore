<?php
add_image_size( 'section-banner', 1088, 500, true );

function kbeshore_output_post_video() {
	if ( $embed_link = get_post_meta( get_the_ID(), '_media_video_embed_link', true ) ) {
		echo wp_oembed_get( $embed_link );
	}
}

/**
 * Return a WP Query object for the Read | Today's Show section.
 *
 */
function kbeshore_read_todays_show_query() {
	return new WP_Query( array(
		'post_type' => 'media',
		'tax_query' => array(
			array(
				'taxonomy' => 'media-category',
				'field' => 'slug',
				'terms' => 'devotional'
			)
		),
		'posts_per_page' => 1
	) );
}

function kbeshore_watch_todays_show_query() {
	return new WP_Query( array(
		'post_type' => 'media',
		'tax_query' => array(
			array(
				'taxonomy' => 'media-category',
				'field' => 'slug',
				'terms' => 'video'
			)
		),
		'posts_per_page' => 1
	) );
}

/**
 * Return a WP Query object for the Listen | Today's Show section.
 *
 */
function kbeshore_listen_todays_show_query() {
	return new WP_Query( array(
		'post_type' => 'media',
		'tax_query' => array(
			array(
				'taxonomy' => 'media-category',
				'field' => 'slug',
				'terms' => 'podcast'
			)
		),
		'posts_per_page' => -1
	) );
}

function has_media_series() {
	$series = wp_get_object_terms(get_the_ID(), array( 'media-series' ) );
	if ( ! empty( $series ) )
		return true;
	else
		return false;
}

function the_media_series() {
	$series = wp_get_object_terms(get_the_ID(), array( 'media-series' ) );
	if ( empty( $series ) )
		return false;
	$series = $series[0];
	echo '<a href="' . get_term_link( $series, 'media-series' ) . '">' . $series->name . '</a>';
}

function kbeshore_get_listen_permalink( $post_id = null ) {
	if ( ! $post_id )
		$post_id = get_the_ID();
	$permalink = get_permalink( $post_id );
	$permalink .= 'listen/';
	return $permalink;
}

function kbeshore_from_series() {
	if ( is_tax( 'media-series' ) )
		return false;
	if ( has_media_series() ) :?>
		<p class="media-series">
			From <?php the_media_series() ?>
			<?php if ( $episode_info = get_post_meta( get_the_ID(), '_media_episode_number', true ) ) : ?>
				episode <?php echo $episode_info; ?>
			<?php endif ?>
		</p>
		<?php
	endif;
}

function kbeshore_subtitle() {
	if ( has_media_series() ) :?>
		<p class="media-series">
			From <?php the_media_series() ?>
			<?php if ( $episode_info = get_post_meta( get_the_ID(), '_media_episode_number', true ) ) : ?>
				episode <?php echo $episode_info; ?>
			<?php endif ?>
		</p>
		<?php
	endif;

	if ( $date = get_post_meta( get_the_ID(), '_media_date', true ) ) : ?>
		<p class="date">
			<?php echo $date ?>
		</p>
		<?php
	endif;
}

/**
 * Whether the post thumbnail should be displayed for the post in the loop.
 */
function kbeshore_show_media_thumbnail() {
	if ( ! has_post_thumbnail() )
		return false;

	// always display thumbs on the main media archive
	if ( is_post_type_archive( 'media' ) )
		return true;

	if ( has_term( 'podcast', 'media-category' ) || has_term( 'video', 'media-category' ) ) {
		if ( is_singular() )
			return true;
		if ( is_tax( 'media-category' ) )
			return true;
		if ( is_tax( 'media-series' ) )
			return false;
	}
	return true;
}

function kbeshore_is_devotional() {
	if ( has_term( 'devotional', 'media-category' ) )
		return true;
	else
		return false;
}

function kbeshore_is_podcast() {
	if ( has_term( 'podcast', 'media-category' ) )
		return true;
	else
		return false;
}

function kbeshore_listen_link() {
	?>
	<a class="button" href="<?php echo kbeshore_get_listen_permalink() ?>" onclick="javascript:void(window.open('<?php echo kbeshore_get_listen_permalink() ?>','kentonbeshorepopupplayer','toolbar=no,location=no,status=no,directories=no,menubar=no,scrolling=auto,scrollbars=auto,width=769,height=846,resizable=yes')); return false;" alt="Listen to this podcast">Listen</a>
	<?php
}

function kbeshore_get_the_media_category( $post_id = null ) {
	if ( ! $post_id )
		$post_id = get_the_ID();

	$terms = wp_get_object_terms( $post_id, array( 'media-category' ) );
	if ( ! empty( $terms ) )
		return $terms[0];

	if ( is_tax( 'media-category' ) ) {
		return get_queried_object();
	}
	return false;
}


function kbeshore_the_media_category() {
	wp_enqueue_style( 'kbeshore-glyphs', KB_MEDIA_URL .'assets/css/icons.css' );
	if ( $term = kbeshore_get_the_media_category() ) : ?>
	<div class="media-type">
		<a class="glyphed" href="<?php echo get_term_link( $term, 'media-category' ) ?>">
			<?php kbeshore_the_media_category_glyph() ?>
		</a>
	</div>
	<?php
	endif;
}

function kbeshore_the_media_category_glyph( $term = null ) {
	if ( ! $term )
		$term = kbeshore_get_the_media_category();

	if ( $term->name == 'video' ) // video
		echo kbeshore_get_glyph( 'video' );
	elseif ( $term->name == 'devotional' ) // devotional
		echo kbeshore_get_glyph( 'devotional' );
	elseif ( $term->name == 'podcast' ) // podcast
		echo kbeshore_get_glyph( 'podcast' );
}

function kbeshore_get_glyph( $glyph ) {
	switch ( $glyph ) {
		case 'facebook':
			return '&#x4a;';
		case 'video':
			return '&#x51;';
		case 'devotional':
			return '&#x4e;';
		case 'podcast':
			return '&#x4f;';
		case 'itunes':
			return '&#x58;';
		case 'rss':
			return '&#x44;';
		case 'stitcher':
			return '&#x45;';
		case 'right-arrow-big':
			return '&#x49;';
		case 'phone':
			return '&#x4b;';
		case 'twitter':
			return '&#x4d;';
		case 'address':
			return '&#x50;';
		case 'vimeo':
			return '&#x53;';
		case 'youtube':
			return '&#x54;';
		case 'right-arrow':
			return '&#x55;';
		case 'left-arrow':
			return '&#x56;';
		case 'media':
			return '&#x52;';
	}
}

function kbeshore_media_description() {
	$description = kbeshore_get_option( 'media_archive_header' );
	$description = stripslashes( $description );
	$description = do_shortcode( $description );
	echo $description;
}

add_action( 'post_class', 'kbeshore_add_media_category_to_post_class', 10, 3 );
function kbeshore_add_media_category_to_post_class( $classes, $class, $post_ID ) {
	$post = get_post( $post_ID );
	if ( 'media' != $post->post_type )
		return;
	$media_category = kbeshore_get_the_media_category();
	if ( empty( $media_category ) )
		return;
	$classes[] = 'media-category-' . $media_category->term_taxonomy_id;
	return $classes;
}

remove_all_actions( 'do_feed_rss2' );
add_action( 'do_feed_rss2', 'kbeshore_feed_rss2', 10, 1 );
function kbeshore_feed_rss2( $for_comments ) {
	$rss_template = KB_MEDIA_PATH . '/includes/feed-media-rss2.php';
	if( get_query_var( 'post_type' ) == 'media' || get_query_var( 'media-category' ) )
		load_template( $rss_template );
	else
		do_feed_rss2( $for_comments ); // Call default function
}

function kbeshore_rss_content() {
	kbeshore_subtitle();
	if ( has_post_thumbnail() )
		the_post_thumbnail( 'large' );
	echo get_the_content_feed('rss2');
	if ( $outline_link = get_post_meta( get_the_ID(), '_media_outline_link', true ) ) { ?>
		<p>Download <a target="_blank" href="<?php echo $outline_link; ?>">Message Outlines</a></p>
	<?php } ?>
	<?php if ( $transcribe_link = get_post_meta( get_the_ID(), '_media_transcribe_link', true ) ) { ?>
		<p>Download <a target="_blank" href="<?php echo $transcribe_link; ?>">Message Transcribe</a></p>
	<?php } ?>
	<?php if ( $flow_questions_link = get_post_meta( get_the_ID(), '_media_flow_questions_link', true ) ) { ?>
		<p>Download <a target="_blank" href="<?php echo $flow_questions_link; ?>">Flow Questions</a></p>
	<?php }
	kbeshore_the_media_category_text();
}

function kbeshore_the_media_category_text() {
	$categories = wp_get_object_terms( get_the_ID(), 'media-category' );
	foreach ( $categories as $category ) {
		$thelist[] = '<a href="' . esc_url( get_term_link( (int)$category->term_id, 'media-category' ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">' . sprintf( __( "View all %s posts" ), $category->name ) . '</a>';
	}
	if ( ! empty( $thelist ) ) {
		echo implode( ' ', $thelist );
	}
}

function kbeshore_get_attachment_image_url() {
	$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
	if ( ! $thumbnail_id )
		return false;

	$image_details = wp_get_attachment_image_src( $thumbnail_id, 'section-banner' );

	return $image_details[0];
}
