<?php

class KB_MEDIA_Hooks {

	public function __construct() {
		add_filter( 'the_content', array( $this, 'filter_media_content' ) );
	}

	function filter_media_content( $content ) {
		$this->content = $content;
		$this->media_category = kbeshore_get_the_media_category();
		if ( ! $this->media_category || ! isset( $this->media_category->name ) ) {
			return $content;
		}

		switch( $this->media_category->name ) {
			case 'podcast':
				return $this->media_single_podcast();
				break;
			case 'video':
				return $this->media_single_video();
				break;
			case 'devotional':
				return $this->media_single_devotional();
				break;
			case 'lent-prayers':
				return $this->media_single_lent();
				break;
			case 'mothers-day':
				return $this->media_single_mothers_day();
				break;
			case 'sermon-clips':
				return $this->media_single_sermonclips();
				break;
			default:
				return $this->media_content();
				break;
		}

		return $content;
	}


	function media_single_podcast() {
		ob_start();
		$this->thumb();
		$this->header();
		?>
		<div class="entry-content">
			<div class="listen-button">
				<?php kbeshore_listen_link() ?>
			</div>
			<div class="download-links">
				<?php if ( $outline_link = get_post_meta( get_the_ID(), '_media_outline_link', true ) ) { ?>
					<p>Download <a target="_blank" href="<?php echo $outline_link; ?>">Message Outlines</a>
				<?php } ?>
				<?php if ( $transcribe_link = get_post_meta( get_the_ID(), '_media_transcribe_link', true ) ) { ?>
					<p>Download <a target="_blank" href="<?php echo $transcribe_link; ?>">Message Transcribe</a></p>
				<?php } ?>
				<?php if ( $flow_questions_link = get_post_meta( get_the_ID(), '_media_flow_questions_link', true ) ) { ?>
					<p>Download <a target="_blank" href="<?php echo $flow_questions_link; ?>">Flow Questions</a></p>
				<?php } ?>
			</div>
			<div class="message-summary">
				<h2>MESSAGE SUMMARY</h2>
				<?php echo $this->content; ?>
			</div>
			<div class="review-and-rate">
				<?php echo kbeshore_get_option( 'rate_this_podcast_text' ) ?>
			</div>
			<div class="music-for-live-boldly">
				<?php echo kbeshore_get_option( 'music_for_this_podcast_text' ) ?>
			</div>
		</div><!-- .entry-content -->
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	function media_single_video() {
		ob_start();
		?>
		<div class="video">
			<?php kbeshore_output_post_video(); ?>
		</div>
		<?php $this->header(); ?>

		<div class="entry-content">
			<div class="download-links">
				<?php if ( $outline_link = get_post_meta( get_the_ID(), '_media_outline_link', true ) ) { ?>
					<p>Download <a target="_blank" href="<?php echo $outline_link; ?>">Message Outline</a>
				<?php } ?>
				<?php if ( $transcribe_link = get_post_meta( get_the_ID(), '_media_transcribe_link', true ) ) { ?>
					<p>Download <a target="_blank" href="<?php echo $transcribe_link; ?>">Message Transcribe</a></p>
				<?php } ?>
				<?php if ( $flow_questions_link = get_post_meta( get_the_ID(), '_media_flow_questions_link', true ) ) { ?>
					<p>Download <a target="_blank" href="<?php echo $flow_questions_link; ?>">Flow Questions</a></p>
				<?php } ?>
			</div>
			<div class="subscribe">
				<h2>SUBSCRIBE</h2>
				<a target="_blank" alt="Youtube Subscribe Link" class="subscribe-link youtube-subscribe-link glyphed" href="<?php echo kbeshore_get_option('subscribe_link_youtube') ?>">
					<?php echo kbeshore_get_glyph( 'youtube' ); ?>
				</a>
				<a target="_blank" alt="Vimeo Subscribe Link" class="subscribe-link vimeo-subscribe-link glyphed" href="<?php echo kbeshore_get_option('subscribe_link_vimeo') ?>">
					<?php echo kbeshore_get_glyph( 'vimeo' ); ?>
				</a>
			</div>
			<div class="message-summary">
				<h2>MESSAGE SUMMARY</h2>
				<?php echo $this->content; ?>
			</div>
		</div><!-- .entry-content -->
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	function media_single_devotional() {
		ob_start();
		$this->thumb();
		$this->header();
		?>
		<div class="entry-content">
			<div class="message-summary">
				<?php echo $this->content; ?>
			</div>
		</div><!-- .entry-content -->
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	function media_single_lent() {
		ob_start();
		$this->thumb();
		$this->header();
		?>
		<div class="entry-content">
			<div class="message-summary">
				<?php echo $this->content; ?>
			</div><br />
			Why <a href="http://kenton.org/media/journey-cross/">lent prayers?</a>
			Get the lent prayers <a href="https://my.leadpages.net/leadbox/14ede2973f72a2%3A142a0e046b46dc/5797971773882368/" target="_blank">via email. </a><script type="text/javascript" src="//my.leadpages.net/leadbox-682.js"></script>
			Previous<a href="http://kenton.org/media-category/lent-prayers/"> prayers </a>
		</div><!-- .entry-content -->
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	function media_single_mothers_day() {
		ob_start();
		if ( kbeshore_show_media_thumbnail() ) : ?>
		<div class="video">
			<?php kbeshore_output_post_mothers-day(); ?>
		</div>
		<div class="featured-image">
			<a href="<?php the_permalink() ?>">
				<img src="<?php echo kbeshore_get_attachment_image_url() ?>" />
			</a>
		</div>
		<?php endif;
		$this->header();
		?>
		<div class="entry-content">
			<?php echo $this->content; ?>
		</div>
		<!-- .entry-content -->
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;

	}

	function media_single_sermonclips() {
		ob_start();
		$this->header();
		?>
		<div class="entry-content">
			<?php echo $this->content; ?>
		</div>
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public function media_content() {
		global $post;
		ob_start();
		$this->thumb();
		$this->header();
		if ( ! is_singular() && $post->excerpt && ! empty( $post->excerpt ) ) : // Only display Excerpts for non-single pages ?>
		<div class="entry-summary">
			<?php echo apply_filters( 'the_excerpt', $post->excerpt ); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
			<?php echo $this->content; ?>
			<?php if ( kbeshore_is_podcast() ) : ?>
				<a class="button" href="javascript:void(window.open('<?php echo kbeshore_get_listen_permalink() ?>','kentonbeshorepopupplayer','toolbar=no,location=no,status=no,directories=no,menubar=no,scrolling=auto,scrollbars=auto,width=640,height=600,resizable=yes')); return false;" alt="Listen to <?php echo esc_attr( get_the_title() ) ?>">Listen</a>
			<?php endif; ?>
		</div><!-- .entry-content -->
		<?php endif;

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public function thumb() {
		if ( ! kbeshore_show_media_thumbnail() ) {
			return '';
		}

		?>
		<div class="featured-image">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo kbeshore_get_attachment_image_url(); ?>" />
			</a>
		</div>
		<?php
	}

	public function header() {
		?>
		<header class="entry-header">
			<?php kbeshore_the_media_category() ?>
			<?php kbeshore_subtitle() ?>
		</header><!-- .entry-header -->
		<?php
	}

}

new KB_MEDIA_Hooks;
