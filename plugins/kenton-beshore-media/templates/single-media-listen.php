<?php
/**
 * The Media template file.
 *
 * This template file is theme agnostic.
 *
 */

$outline_link = get_post_meta( get_the_ID(), '_media_outline_link', true );
$audio_link = get_post_meta( get_the_ID(), '_media_audio_link', true );

function get_file_extension_from_url( $url ) {
	$bits = explode( '.', $url );
	return $bits[ count($bits) - 1 ];
}
 ?>
 <html>
 <head>
 	<title><?php wp_title(); ?></title>
 	<?php global $wp_scripts, $wp_styles;
 	wp_register_script( 'jplayer', KB_MEDIA_URL . '/includes/javascript/jQuery.jPlayer.2.3.0/jquery.jplayer.min.js', array( 'jquery' ) );
 	wp_register_style( 'jplayer', KB_MEDIA_URL . '/includes/javascript/jQuery.jPlayer.2.3.0/blue.monday/jplayer.blue.monday.css' );
 	$wp_scripts->print_scripts( 'jplayer' );
 	$wp_styles->do_item( 'jplayer' ); ?>
 	<script type="text/javascript">
	(function($) {
		$(document).ready(function(){
		  	$("#jquery_jplayer_1").jPlayer({
			ready: function () {
			  	$(this).jPlayer("setMedia", {
			  		<?php if ( $audio_link ) echo get_file_extension_from_url( $audio_link ) .': "' . $audio_link . '"'; ?>
			  	});
			},
			swfPath: "<?php echo KB_MEDIA_URL . '/includes/javascript/jQuery.jPlayer.2.3.0/'; ?>",
			supplied: "<?php echo get_file_extension_from_url( $audio_link ); ?>"
		  });
		});
	})(jQuery);
  </script>
 </head>
 <body>

 </body>
 </html>

	<div id="primary" class="site-content">
		<div id="content" role="main">
		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_title(); ?>
				<?php if ( $audio_link = get_post_meta( get_the_ID(), '_media_audio_link', true ) ) { ?>
				<div id="jp_container_1" class="jp-video ">
				    <div class="jp-type-single">
				      <div id="jquery_jplayer_1" class="jp-jplayer"></div>
				      <div class="jp-gui">
				        <div class="jp-video-play">
				          <a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
				        </div>
				        <div class="jp-interface">
				          <div class="jp-progress">
				            <div class="jp-seek-bar">
				              <div class="jp-play-bar"></div>
				            </div>
				          </div>
				          <div class="jp-current-time"></div>
				          <div class="jp-duration"></div>
				          <div class="jp-controls-holder">
				            <ul class="jp-controls">
				              <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
				              <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
				              <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
				              <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
				              <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
				              <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
				            </ul>
				            <div class="jp-volume-bar">
				              <div class="jp-volume-bar-value"></div>
				            </div>
				            <ul class="jp-toggles">
				              <li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
				              <li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
				              <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
				              <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
				            </ul>
				          </div>
				        </div>
				      </div>
				      <div class="jp-no-solution">
				        <span>Update Required</span>
				        To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
				      </div>
				    </div>
				  </div>
				<?php } ?>
				<?php if ( $outline_link ) { ?>
					<a href="<?php echo $outline_link; ?>">Outline link</a>
				<?php } ?>
				<?php if ( $transcribe_link = get_post_meta( get_the_ID(), '_media_transcribe_link', true ) ) { ?>
					<a href="<?php echo $transcribe_link; ?>">Transcribe Link</a>
				<?php } ?>
				<?php if ( $subscribe_to_podcast_link = get_post_meta( get_the_ID(), '_media_subscribe_to_podcast_link', true ) ) { ?>
					<a href="<?php echo $subscribe_to_podcast_link; ?>">Subscribe to Podcast Link</a>
				<?php } ?>
				<?php if ( $buy_message_link = get_post_meta( get_the_ID(), '_media_buy_message_link', true ) ) { ?>
					<a href="<?php echo $buy_message_link; ?>">Buy Message Link</a>
				<?php } ?>
				<?php if ( $request_our_current_offer_link = get_post_meta( get_the_ID(), '_media_request_our_current_offer_link', true ) ) { ?>
					<a href="<?php echo $request_our_current_offer_link; ?>">Request our current offer link</a>
				<?php } ?>
			<?php endwhile; ?>

		<?php endif; // end have_posts() check ?>

		</div><!-- #content -->
	</div><!-- #primary -->
</html>
