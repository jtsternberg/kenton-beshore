<?php
/*
Plugin Name: Kenton Beshore Media
Description: Adds support for a media post type.
Author: Eric Andrew Lewis
Version: 0.1
Author URI: http://www.ericandrewlewis.com
*/

define( 'KB_MEDIA_URL', trailingslashit( plugins_url( '', __FILE__ ) ) );
define( 'KB_MEDIA_PATH', dirname( __FILE__ ) . '/' );

if ( 'kenton-beshore' != get_stylesheet() ) {
	require_once( KB_MEDIA_PATH . 'includes/functions.php' );
	require_once( KB_MEDIA_PATH . 'includes/hooks.php' );
	require_once( KB_MEDIA_PATH . 'includes/settings.php' );
}

/**
 * Activation hook. Flush rewrites.
 *
 * @since 0.1.0
 */
function media_activate() {
	media_add_rewrite_rules();
	media_register_taxonomies();
	media_register_post_types();
	flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'media_activate' );

/**
 * Deactivation hook. Flush rewrites.
 *
 * @since 0.1.0
 */
function media_deactivate() {
	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'media_deactivate' );


add_action( 'init', 'media_register_post_types' );

/**
 * Require media item custom post type
 *
 */
function media_register_post_types() {
	$args = array(
		'labels' => array(
			'name' => 'Media items',
			'singular_name' => 'Media item',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Media item',
			'edit_item' => 'Edit Media item',
			'new_item' => 'New Media item',
			'all_items' => 'All Media items',
			'view_item' => 'View Media item',
			'search_items' => 'Search Media items',
			'not_found' =>	'No Media items found',
			'not_found_in_trash' => 'No Media items found in Trash',
			'parent_item_colon' => '',
			'menu_name' => 'Media Items'
		),
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => 'player',
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'thumbnail', 'editor', 'comments' )
	);

	register_post_type( 'media', $args );
}

add_action( 'init', 'media_include_cmb', 9998 );

/**
 * Require Custom Meta Boxes library
 *
 */
function media_include_cmb() {
	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once( KB_MEDIA_PATH . 'cmb/init.php' );
}

add_filter( 'cmb_meta_boxes', 'media_metaboxes' );

/**
 * Define the metabox and field configurations for media custom post type.
 *
 * @param  array $meta_boxes
 * @return array
 *
 * @since 0.1.0
 */
function media_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_media_';

	$meta_boxes[] = array(
		'id'         => 'media_metabox',
		'title'      => 'Media Details',
		'pages'      => array( 'media', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Air Date',
				'desc' => '',
				'id'   => $prefix . 'airdate',
				'type' => 'text',
			),
			array(
				'name' => 'Audio Embed Link',
				'desc' => 'For use with the embedded audio player',
				'id'   => $prefix . 'audio_link',
				'type' => 'text',
			),
			array(
				'name' => 'Video Embed Link',
				'desc' => 'For use with the embedded video player (e.g. http://youtube.com/vndfj37h/',
				'id'   => $prefix . 'video_embed_link',
				'type' => 'text',
			),
			array(
				'name' => 'Outline Download Link',
				'desc' => 'For an outline hosted on another website, generates a download link.',
				'id'   => $prefix . 'outline_link',
				'type' => 'text',
			),
			array(
				'name' => 'Transcribe Download Link',
				'desc' => 'For when the transcribe is hosted on another website',
				'id'   => $prefix . 'transcribe_link',
				'type' => 'text',
			),
			array(
				'name' => 'Subscribe to Podcast Link',
				'desc' => '',
				'id'   => $prefix . 'subscribe_to_podcast_link',
				'type' => 'text',
			),
			array(
				'name' => 'Buy Message Link',
				'desc' => '',
				'id'   => $prefix . 'buy_message_link',
				'type' => 'text',
			),
			array(
				'name' => 'Request our current offer Link',
				'desc' => '',
				'id'   => $prefix . 'request_our_current_offer_link',
				'type' => 'text',
			),
			array(
				'name' => 'Flow Questions Link',
				'desc' => '',
				'id'   => $prefix . 'flow_questions_link',
				'type' => 'text',
			),
			array(
				'name' => 'Episode Number',
				'desc' => 'e.g. 3 of 12',
				'id'   => $prefix . 'episode_number',
				'type' => 'text',
			),
			array(
				'name' => 'Date',
				'desc' => 'e.g. DAY 2 week of Oct 8 – Oct 14',
				'id'   => $prefix . 'date',
				'type' => 'text',
			),
		),

	);

	return $meta_boxes;
}

add_action( 'init', 'media_register_taxonomies' );

/**
 * Register custom taxonomies Sermon Tags, Pastors, and Series
 *
 * @since 0.1.0
 */
function media_register_taxonomies() {
	$args = array(
		'hierarchical'      => true,
		'labels'            => array(
			'name'              => _x( 'Media Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Media Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Media Categories' ),
			'all_items'         => __( 'All Media Categories' ),
			'parent_item'       => __( 'Parent Media Category' ),
			'parent_item_colon' => __( 'Parent Media Category:' ),
			'edit_item'         => __( 'Edit Media Category' ),
			'update_item'       => __( 'Update Media Category' ),
			'add_new_item'      => __( 'Add New Media Category' ),
			'new_item_name'     => __( 'New Media Category Name' ),
			'menu_name'         => __( 'Media Category' )
		),
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
	);

	register_taxonomy( 'media-category', array( 'media' ), $args );

	$args = array(
		'hierarchical'      => true,
		'labels'            => array(
			'name'              => _x( 'Media Series', 'taxonomy general name' ),
			'singular_name'     => _x( 'Media Series', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Media Series' ),
			'all_items'         => __( 'All Media Series' ),
			'parent_item'       => __( 'Parent Media Series' ),
			'parent_item_colon' => __( 'Parent Media Series:' ),
			'edit_item'         => __( 'Edit Media Series' ),
			'update_item'       => __( 'Update Media Series' ),
			'add_new_item'      => __( 'Add New Media Series' ),
			'new_item_name'     => __( 'New Media Series Name' ),
			'menu_name'         => __( 'Media Series' )
		),
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
	);

	register_taxonomy( 'media-series', array( 'media' ), $args );
}

add_filter( 'template_include', 'media_load_custom_template', 10, 1 );

/**
 * Load the pop-up window template for a media item.
 *
 * @since 0.1.0
 */
function media_load_custom_template( $template ) {
	if ( ! is_singular( 'media' ) )
		return $template;

	// If we're loading the Listen template
	if ( get_query_var( 'media_view' ) && get_query_var('media_view') == 'listen' ) {
		add_filter('body_class', 'media_filter_body_class_listen_template' );
		if ( $template = locate_template( 'single-media-listen.php' ) ) {
			return $template;
		} else {
			return KB_MEDIA_PATH . 'templates/single-media-listen.php';
		}
	}

	// Otherwise, load the theme's single.
	return $template;
}

function media_filter_body_class_listen_template($classes) {
	$classes[] = 'post-type-media-listen';
	return $classes;
}

add_action( 'init', 'media_add_rewrite_rules' );
function media_add_rewrite_rules() {
	add_rewrite_rule('media/(.*)/listen/?$', 'index.php?media=$matches[1]&media_view=listen', 'top' );
}

add_filter( 'query_vars', 'media_add_query_var', 10, 1 );
function media_add_query_var($vars ) {
    $vars[] = 'media_view'; // var1 is the name of variable you want to add
    return $vars;
}
