<div class="section-content">
	<div class="section-secondary">
		<table class="contact-method-table">
			
			<tr>
				<td class="contact-method-icon" width="85">
					<?php echo kenton_beshore_get_glyph( 'twitter' ) ?>
				</td>
				<td class="contact-method-details">
					<a target="_blank" alt="Kenton Beshore Twitter" href="http://twitter.com/<?php echo kenton_beshore_get_theme_option('twitter_handle') ?>">
						&#64;KENTON BESHORE
					</a>
				</td>
			</tr>
			<tr>
				<td class="contact-method-icon" width="85">
					<?php echo kenton_beshore_get_glyph( 'facebook' ) ?>
				</td>
				<td class="contact-method-details">
					<a target="_blank" alt="Kenton Beshore Facebook" href="<?php echo kenton_beshore_get_theme_option('facebook_link') ?>">
					KENTON ON<br />FACEBOOK
					</a>
				</td>
			</tr>
			<tr>
				<td class="contact-method-icon" width="85">
					<?php echo kenton_beshore_get_glyph( 'address' ) ?>
				</td>
				<td class="contact-method-details">
					<?php echo kenton_beshore_get_theme_option('address') ?>
				</td>
			</tr>
		</table>
	</div><!-- .section-secondary -->
	<div class="section-primary">
		<?php the_content(); ?>
	</div><!-- .section-primary -->
</div><!-- #section-content -->