<?php $watch_query = kenton_beshore_watch_todays_show_query(); ?>
<?php while( $watch_query->have_posts() ) : $watch_query->the_post(); ?>
<div class="section-content">
	<div class="section-title-wrap">
		</div>
	<div class="section-primary">
		<div class="video">
			<?php kenton_beshore_output_post_video(); ?>
		</div>
		<h2><?php the_title() ?></h2>
		<?php kenton_beshore_from_series() ?>
		<?php the_excerpt() ?>
		<a class="button" href="<?php echo get_permalink( get_the_ID() ) ?>">READ MORE</a>
		<div class="subscribe-link-group">
			<p>Subscribe via</p>
			<a target="_blank" alt="Youtube Subscribe Link" class="subscribe-link youtube-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_youtube') ?>"><?php echo kenton_beshore_get_glyph( 'youtube' ); ?></a>
			<a target="_blank" alt="Vimeo Subscribe Link" class="subscribe-link vimeo-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_vimeo') ?>"><?php echo kenton_beshore_get_glyph( 'vimeo' ); ?></a>
		</div>
	</div>
</div><!-- #section-content -->
<?php endwhile; ?>
<?php wp_reset_postdata() ?>

