<?php
function kenton_beshore_tweet_date( $date_string ) {
	$date = DateTime::createFromFormat('D M d H:i:s O Y', $date_string );
	echo $date->format('F dS');
}

function kenton_beshore_tweet_content( $src, $target = '_blank', $alreadyhtml = false ){
	if( ! $alreadyhtml ){
		$src = esc_html( $src );
	}
	// linkify URLs (restricting to 30 chars as per twitter.com)
	$src = preg_replace_callback('!(https?://)(\S+)!', 'kenton_beshore_linkify_callback', $src );
	if( '_blank' !== $target ){
		$src = str_replace( '"_blank"', '"'.$target.'"', $src );
	}
	// linkify @names
	$src = preg_replace('!@([a-z0-9_]{1,15})!i', '<a class="twitter-screen-name" href="https://twitter.com/\\1" target="'.$target.'">\\0</a>', $src );
	// linkify #hashtags
	$src = preg_replace('/(?<!&)#(\w+)/i', '<a class="twitter-hashtag" href="https://twitter.com/search?q=%23\\1&amp;src=hash" target="'.$target.'">\\0</a>', $src );
	echo $src;
}

function kenton_beshore_linkify_callback( array $r ){
    list( , $proto, $label ) = $r;
    $label = str_replace( '#', '&#35;', $label );
    $href = $proto.$label;
    if( isset($label{30}) ){
        $label = substr_replace( $label, '&hellip;', 30 );
    }
    return '<a href="'.$href.'" target="_blank">'.$label.'</a>';
}


?>

<div class="section-content">
	<h1 class="section-title"><?php the_title() ?></h1>
	<div class="twitter-link">
		<a alt="Kenton Beshore Twitter" href="http://twitter.com/<?php echo kenton_beshore_get_theme_option('twitter_handle') ?>">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAMAAAC5KTl3AAAAM1BMVEUAAAAIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0oIK0pg0jLLAAAAEHRSTlMAECAwQFBgcICPn6+/z9/vIxqCigAAAiNJREFUWIXNV926rSAI3Lr8z9L3f9qzrNwFQmrnZs9lH04Iw0Q/P38aUrsQD3inxOxxHbYMsfrPxNt9yhQ2O5aJDOTxHcn1OYTnz+8cpkOg6PzviPKJoJPAmYbmbxBHCL5wHME6SJBz+F8CmmKKgKQYrUGFfdUFAAUJ1DRB3oA8BZ6jEYBSuBcE4B6yH52IeYsXw8M0Hgha2I9uHpvhFL43Fo7QS2Sq0FR13auuCN+ptoWOOGHgk1qx1jvOdnzQ4628cGnCCgwKTYwcd8HKUJO+Owpu+5FeW6JD88IllEIbvDuFaAjyeo6NXFAKjfwjPRL+OmDvsqHKfgRhpNvMGGyKqGqSKE6uAqAhlLG3l5ZSUtaS3IOlA2lpjsErlsAuMV4UjmHg7PyA9LnLkPgUTASS4BjKPDMkC4zjGbibYCcoUYy9YCWdoAaD8chEriwSe0RhwBN7ZUF8oxccVB5iewBXxI3AMdv++JbYdpU1Er1oh3BpMgvKl3avpCZ1a5SHW4PhDPxQtUNczQO2eGXWRknpZqUL7Ik0mBWxrgCNAweYhzBNE8/A33e1X+7N64NFacfvJpcJs6Lq4GZDb9YHKLkXKww0ZELvA4Bj82INwvsgO18c2i/CZD8o+6BkP0UwsE31CCYuwhGQBkCBG9+9IyML/oOFFHS3243/RzohH+U58rv4HRK2KWPn9zwcJfLFTP18S7vcWdYwd7xCKeWcVerpH/UfdNua/ZdehMgAAAAASUVORK5CYII=" width="65" height="65" alt="Kenton Beshore Twitter" />
		</a>
	</div>
	<div class="section-primary">
		<?php foreach( kenton_beshore_get_tweets() as $tweet ) : ?>
			<div class="tweet">
				<div class="tweet-content">
				<?php kenton_beshore_tweet_content( $tweet['text'])  ?>
				</div>
				<div class="tweet-date">
					<?php kenton_beshore_tweet_date( $tweet['created_at'] ) ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div><!-- #section-content -->