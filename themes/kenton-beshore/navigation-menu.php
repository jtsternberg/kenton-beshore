<header id="masthead" class="site-header" role="banner">
	<div class="hamburger">
		<div></div>
		<div></div>
		<div></div>
	</div>
	<a href="<?php echo site_url() ?>" alt="Kenton Beshore Homepage">
		<div class="site-logo">
			<?php if ( kenton_beshore_use_blue_logo_in_header() ) : ?>
			<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/empty.png' ?>" alt="Kenton Beshore logo" />
			<?php else : ?>
			<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/empty.png' ?>" alt="Kenton Beshore logo" />
			<?php endif; ?>
		</div>
	</a>
	<nav id="site-navigation" class="main-navigation" role="navigation">
		<a href="#" class="nav-toggle" id="nav-close-btn"></a>
		<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'kenton-beshore' ); ?>"><?php _e( 'Skip to content', 'kenton-beshore' ); ?></a></div>
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'walker' => new Kenton_Beshore_Walker_Nav_Menu ) ); ?>
	</nav><!-- #site-navigation -->
</header><!-- #masthead -->