<?php
/**
 * kenton-beshore functions and definitions
 *
 * @package kenton-beshore
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/**
 * General runtime theme-specific data stored in this global.
 */
global $kenton_beshore;
if ( empty( $kenton_beshore ) )
	$kenton_beshore = new StdClass;

if ( ! function_exists( 'kenton_beshore_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function kenton_beshore_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on kenton-beshore, use a find and replace
	 * to change 'kenton-beshore' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'kenton-beshore', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'kenton-beshore' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	 */
	add_theme_support( 'custom-background', apply_filters( 'kenton_beshore_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	add_image_size( '300square', 300, 300, true );
	add_image_size( 'section-banner', 1088, 500, true );

}
endif; // kenton_beshore_setup
add_action( 'after_setup_theme', 'kenton_beshore_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function kenton_beshore_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'kenton-beshore' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'kenton_beshore_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function kenton_beshore_scripts() {
	wp_enqueue_style( 'kenton-beshore-style', get_stylesheet_uri() );

	//wp_enqueue_script( 'kenton-beshore-navigation', get_template_directory_uri() . '/js/navigation.js', array(), NULL, true );

	wp_register_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js' );

	if ( ! is_admin() )
		wp_enqueue_script( 'kenton-beshore-common', get_template_directory_uri() . '/js/common.js', array('jquery', 'modernizr'), NULL, true );

	//wp_enqueue_script( 'kenton-beshore-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), NULL, true );

	wp_enqueue_script( 'jquery-textfill', get_template_directory_uri() . '/js/jquery.textfill.js', array( 'jquery' ), NULL );

	wp_enqueue_script( 'hamburger-js', get_template_directory_uri() . '/inc/hamburger-menu/hamburger.js', array( 'jquery', 'jquery-ui-core', 'jquery-effects-core' ), NULL );

	wp_enqueue_script( 'jquery-scrollspy', get_template_directory_uri() . '/js/jquery.scrollspy.js', array( 'jquery' ) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'kenton-beshore-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

	if ( is_404() || is_front_page() ) {
		wp_enqueue_script( 'jquery-reveal', get_template_directory_uri() . '/js/reveal/jquery.reveal.js', array( 'jquery' ) );
		wp_enqueue_style( 'jquery-reveal', get_template_directory_uri() . '/js/reveal/reveal.css' );
		//wp_enqueue_style( 'kenton-beshore-reveal', get_template_directory_uri() . '/js/reveal.js' );
	}

	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/assets/genericons/genericons.css' );
	$common_data = array(
		'theme_url' => get_stylesheet_directory_uri(),
		'use_blue_logo' => kenton_beshore_use_blue_logo_in_header()
	);
    wp_localize_script( 'kenton-beshore-common', 'common_data', $common_data );
}
add_action( 'wp_enqueue_scripts', 'kenton_beshore_scripts' );

/**
 * Custom metaboxes.
 */
require get_template_directory() . '/inc/navigation-menu-walker.php';

/**
 * Custom metaboxes.
 */
require get_template_directory() . '/inc/custom-metaboxes.php';

/**
 * Theme Settings.
 */
require get_template_directory() . '/inc/theme-settings.php';

/**
 * Page sections.
 */
require get_template_directory() . '/inc/page-sections/page-sections.php';

/**
 * Color palettes.
 */
require get_template_directory() . '/inc/page-sections/color-palettes.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


function have_page_sections() {
	global $page_sections_query;
	if ( empty( $page_sections_query ) )
		kenton_beshore_setup_page_sections_query();

	if ( $page_sections_query->have_posts() )
		return $page_sections_query->have_posts();
	else
		wp_reset_postdata();
}

function the_page_section() {
	global $page_sections_query;
	return $page_sections_query->the_post();
}

function kenton_beshore_setup_page_sections_query() {
	global $page_sections_query;
	$_sections = get_post_meta( get_the_ID(), '_kenton_beshore_page_sections', true );
	$sections = explode( ',', $_sections );
	$page_sections_query = new WP_Query( array(
		'post_type' => 'page-section',
		'posts_per_page' => -1,
		'post__in' => $sections,
		'orderby' => 'post__in'
	) );
}

function kenton_beshore_get_section_layout() {
	$layout = get_post_meta( get_the_ID(), '_kenton_beshore_section_layout_type', true );
	if ( ! $layout )
		$layout = 'two-column-content-right';
	return $layout;
}

function kenton_beshore_get_section_color_palette() {
	if ( is_404() )
		$color_palette = 2;
	else
		$color_palette = get_post_meta( get_the_ID(), '_kenton_beshore_color_palette', true );

	if ( ! $color_palette )
		$color_palette = 1;
	return $color_palette;
}

function kenton_beshore_load_page_section_template() {
	locate_template( 'page-section-' . kenton_beshore_get_section_layout() . '.php', true, false );
}

function kenton_beshore_output_post_video() {
	if ( $embed_link = get_post_meta( get_the_ID(), '_media_video_embed_link', true ) ) {
		echo wp_oembed_get( $embed_link );
	}
}

function kenton_beshore_get_blog_url() {
	// If a page is set to show the blog index, return that.
	if ( get_option( 'page_for_posts' ) )
		return get_permalink( get_option( 'page_for_posts' ) );

	// If there is a page on front, there is no blog index.
	if ( 'posts' === get_option( 'show_on_front' ) )
		return site_url();

	// Otherwise, there is no blog index.
	return false;
}

/**
 * Get latest tweets.
 *
 * @param type $screen_name
 * @param type $count
 * @param type $rts
 * @param type $ats
 * @return type
 */
function kenton_beshore_get_tweets( $screen_name = 'kentonbeshore', $count = 4, $rts = false, $ats = false ) {
	if( ! function_exists('twitter_api_get') ){
		require_once WP_CONTENT_DIR . '/plugins/latest-tweets-widget/lib/twitter-api.php';
		_twitter_api_init_l10n();
	}
	// caching full data set, not just twitter api caching
	$cachettl = (int) apply_filters('latest_tweets_cache_seconds', 300 );
	if( $cachettl ){
		$arguments = func_get_args();
		$cachekey = 'latest_tweets_'.implode('_', $arguments );
		if( ! function_exists('_twitter_api_cache_get') ){
			twitter_api_include('core');
		}
		if( $rendered = _twitter_api_cache_get($cachekey) ){
			return $rendered;
		}
	}
	// Build API params for "statuses/user_timeline" // https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline
	$trim_user = false;
	$include_rts = ! empty($rts);
	$exclude_replies = empty($ats);
	$params = compact('exclude_replies','include_rts','trim_user','screen_name');
	// Stripping tweets means we may get less than $count tweets.
	// we'll keep going until we get the amount we need, but may as well get more each time.
	if( $exclude_replies || ! $include_rts ){
		$params['count'] = $count * 3;
	}
	// else ensure we always get more than one to avoid infinite loop on max_id bug
	else {
		$params['count'] = max( 2, $count );
	}
	// pull tweets until we either have enough, or there are no more
	$tweets = array();
	while( $batch = twitter_api_get('statuses/user_timeline', $params ) ){
		$max_id = null;
		foreach( $batch as $tweet ){
			if( isset($params['max_id']) && $tweet['id_str'] === $params['max_id'] ){
				// previous max included in results, even though docs say it won't be
				continue;
			}
			$max_id = $tweet['id_str'];
			if( ! $include_rts && preg_match('/^(?:RT|MT)[ :\-]*@/i', $tweet['text']) ){
			// skipping manual RT
				continue;
			}
			$tweets[] = $tweet;
		}
		if( isset($tweets[$count]) ){
			$tweets = array_slice( $tweets, 0, $count );
			break;
		}
		if( ! $max_id ){
			// infinite loop would occur if user had only tweeted once, ever.
			break;
		}
		$params['max_id'] = $max_id;
	}
	return $tweets;
}

/**
 * Dynamic <footer> class output.
 */
function kenton_beshore_get_footer_class() {
	$footer_color_palette = get_post_meta( get_the_ID(), '_kenton_beshore_color_palette', true );
	if ( ! $footer_color_palette )
		$footer_color_palette = 1;
	return $footer_color_palette;
}

function kenton_beshore_get_theme_option( $option_name ) {
	$options = get_option( 'kenton_beshore_theme_settings' );
	if ( ! empty( $options[$option_name] ) )
		return $options[$option_name];
	else
		return false;
}

function kenton_beshore_set_theme_option( $option_name, $value ) {
	$options = get_option( 'kenton_beshore_theme_settings' );
	if ( empty( $options ) )
		$options = array();
	$options[$option_name] = $value;
	update_option( 'kenton_beshore_theme_settings', $options );
}

function kenton_beshore_get_extra_theme_option( $option_name ) {
	$options = get_option( 'kenton_beshore_extra_theme_settings' );
	if ( ! empty( $options[$option_name] ) )
		return $options[$option_name];
	else
		return false;
}

function kenton_beshore_set_extra_theme_option( $option_name, $value ) {
	$options = get_option( 'kenton_beshore_extra_theme_settings' );
	if ( empty( $options ) )
		$options = array();
	$options[$option_name] = $value;
	update_option( 'kenton_beshore_extra_theme_settings', $options );
}
/**
 * Setup the latest media global for the Listen | Today's Show template.
 */
function setup_latest_media_global() {
	global $latest_media_query;
	if ( empty( $latest_media_query ) ) {
		$latest_media_query = new WP_Query( array(
			'post_type' => 'media',
			'posts_per_page' => 1,
			'tax_query' => array(
				array(
					'taxonomy' => 'media-category',
					'field' => 'slug',
					'terms' => 'podcast'
				)
			),
		) );
	}

	if ( $latest_media_query->have_posts() )
		$latest_media_query->the_post();
	else
		return false;
}

/**
 *
 * @global type $latest_media_query
 * @return type
 */
function get_latest_media_global_id() {
	global $latest_media_query;
	return $latest_media_query->post->ID;
}

/**
 * Get the excerpt for a given $post_id.
 *
 * @param type $post_id
 * @return type
 */
function kenton_beshore_get_the_excerpt( $post_id ) {
	$post = get_post( $post_id );
	return apply_filters( 'get_the_excerpt', $post->post_excerpt );
}

add_filter( 'excerpt_length', 'kenton_beshore_increase_excerpt_length' );

function kenton_beshore_increase_excerpt_length( $excerpt_length ) {
	return 500;
}

function sanitize_phone( $phone_number ) {
	$phone_number = preg_replace( '/[^0-9]/', '', $phone_number );
	return $phone_number;
}

add_filter( 'get_the_excerpt', 'kenton_beshore_custom_excerpt' );

/**
 * Custom excerpt at the first break after 270 chars
 *
 * @param type $post_content
 * @return string
 */
function kenton_beshore_custom_excerpt( $post_content ) {
	if ( strlen( $post_content ) > 270 )
		$excerpt = substr( $post_content, 0, strpos( $post_content, ' ', 270 ) );
	else
		$excerpt = $post_content;
	$excerpt .= '...';
	return $excerpt;
}

/**
 * Find the nth occurence of a string within a string.
 *
 * @param type $haystack
 * @param type $needle
 * @param type $n
 */
function strpos_n( $haystack, $needle, $n ) {
	$_haystack = $haystack;
	$needle_offset = 0;
	for ( $i = 1; $i <= $n; $i++ ) {
		$str_offset = strpos( $_haystack, $needle );
		if ( ! $str_offset )
			return false;
		$current_offset = $str_offset;
		$needle_offset += $current_offset;
		if ( $i > 1 )
			$needle_offset += strlen( $needle );

		$_haystack = substr( $_haystack, $current_offset + strlen( $needle ) );
		if ( $n == $i )
			return $needle_offset;
	}
}

/**
 * Return a WP Query object for the Read | Today's Show section.
 *
 */
function kenton_beshore_read_todays_show_query() {
	return new WP_Query( array(
		'post_type' => 'media',
		'tax_query' => array(
			array(
				'taxonomy' => 'media-category',
				'field' => 'slug',
				'terms' => 'devotional'
			)
		),
		'posts_per_page' => 1
	) );
}

function kenton_beshore_watch_todays_show_query() {
	return new WP_Query( array(
		'post_type' => 'media',
		'tax_query' => array(
			array(
				'taxonomy' => 'media-category',
				'field' => 'slug',
				'terms' => 'video'
			)
		),
		'posts_per_page' => 1
	) );
}


/**
 * Return a WP Query object for the Listen | Today's Show section.
 *
 */
function kenton_beshore_listen_todays_show_query() {
	return new WP_Query( array(
		'post_type' => 'media',
		'tax_query' => array(
			array(
				'taxonomy' => 'media-category',
				'field' => 'slug',
				'terms' => 'podcast'
			)
		),
		'posts_per_page' => -1
	) );
}

/**
 * Output the inline CSS for the featured image banner of posts.
 */
function kenton_beshore_output_featured_image_banner_style() {
	$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
	if ( ! $thumbnail_id )
		return false;

	$image_details = wp_get_attachment_image_src( $thumbnail_id, 'section-banner' );

	echo "background-image: url( '{$image_details[0]}'); ";
}

function kenton_beshore_get_attachment_image_url() {
	$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
	if ( ! $thumbnail_id )
		return false;

	$image_details = wp_get_attachment_image_src( $thumbnail_id, 'section-banner' );

	return $image_details[0];
}

function kenton_beshore_queried_object_is_page() {
	$queried_object = get_queried_object();
	if ( is_a( $queried_object, 'WP_Post' ) && $queried_object->post_type == 'page' )
		return true;
	else
		return false;
}

function kenton_beshore_load_menu() {
	locate_template( 'navigation-menu.php', true, false );
}

/**
 * Enqueue custom stylesheet for mediaelement.
 *
 */
function kenton_enqueue_mediaelement_styles( $shortcode_markup ) {
	wp_enqueue_style( 'kenton-beshore-mediaelement', get_stylesheet_directory_uri() . '/js/mediaelement/mediaelement.css', array( 'wp-mediaelement' ) );
	return $shortcode_markup;
}

// add_filter( 'wp_audio_shortcode', 'kenton_enqueue_mediaelement_styles' );


function has_media_series() {
	$series = wp_get_object_terms(get_the_ID(), array( 'media-series' ) );
	if ( ! empty( $series ) )
		return true;
	else
		return false;
}

function the_media_series() {
	$series = wp_get_object_terms(get_the_ID(), array( 'media-series' ) );
	if ( empty( $series ) )
		return false;
	$series = $series[0];
	echo '<a href="' . get_term_link( $series, 'media-series' ) . '">' . $series->name . '</a>';
}

/**
 * Output the second column's content.
 */
function kenton_beshore_the_content_second_column() {
	$content = get_post_meta( get_the_ID(), '_kenton_beshore_second_column', true );
	$content = do_shortcode($content);
	echo wpautop($content);
}

function kenton_beshore_show_footer() {
	$show_footer = get_post_meta( get_the_ID(), '_kenton_beshore_show_footer', true );
	if ( $show_footer == 'on' )
		return true;
	if ( is_post_type_archive() )
		return true;
	if ( is_singular( 'media' ) )
		return true;
	if ( is_tax() )
		return true;
	// if ( is_taxonomy_terms_archive() )
		// return true;
	else
		return false;
}

function kenton_beshore_get_listen_permalink( $post_id = null ) {
	if ( ! $post_id )
		$post_id = get_the_ID();
	$permalink = get_permalink( $post_id );
	$permalink .= 'listen/';
	return $permalink;
}

function kenton_beshore_from_series() {
	if ( is_tax( 'media-series' ) )
		return false;
	if ( has_media_series() ) :?>
		<p class="media-series">
			From <?php the_media_series() ?>
			<?php if ( $episode_info = get_post_meta( get_the_ID(), '_media_episode_number', true ) ) : ?>
				episode <?php echo $episode_info; ?>
			<?php endif ?>
		</p>
		<?php
	endif;
}

function kenton_beshore_subtitle() {
	if ( has_media_series() ) :?>
		<p class="media-series">
			From <?php the_media_series() ?>
			<?php if ( $episode_info = get_post_meta( get_the_ID(), '_media_episode_number', true ) ) : ?>
				episode <?php echo $episode_info; ?>
			<?php endif ?>
		</p>
		<?php
	endif;

	if ( $date = get_post_meta( get_the_ID(), '_media_date', true ) ) : ?>
		<p class="date">
			<?php echo $date ?>
		</p>
		<?php
	endif;
}
/**
 * Whether we should display the dark blue logo in the header.
 *
 */
function kenton_beshore_use_blue_logo_in_header() {
	if ( 5 == kenton_beshore_get_section_color_palette() )
		return true;
	if ( is_post_type_archive() )
		return true;
	// if ( is_taxonomy_terms_archive() )
		// return true;
	if ( is_singular( 'media' ) && ! get_query_var('media_view' ) )
		return true;
	if ( is_tax() )
		return true;

	return false;
}

add_action( 'after_section', 'kenton_beshore_output_kentons_list_lightbox' );

/**
 * Output the markup for the Kenton's List lightbox, used on the hompage and the 404 page.
 *
 */
function kenton_beshore_output_kentons_list_lightbox() {
	if ( get_the_ID() != 17 )
		return;
	$post = get_post( 387 );
	?>
	<div id="kentons-list-modal" class="reveal-modal">
		<a class="close-reveal-modal"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/close-reveal.png' ?>" width="36" height="36" alt="" /></a>
		<div class='section-primary'>
			<?php echo apply_filters( 'the_content', $post->post_content ); ?>
		</div>
		<div class='section-secondary'>
			<?php echo apply_filters( 'the_content', get_post_meta( $post->ID, '_kenton_beshore_second_column', true ) ); ?>
		</div>
	</div>
	<?php
}

add_action( 'media-series_edit_form_fields', 'kenton_beshore_edit_term_extra_form_fields', 10, 2 );
add_action( 'media-category_edit_form_fields', 'kenton_beshore_edit_term_extra_form_fields', 10, 2 );

function kenton_beshore_edit_term_extra_form_fields( $tag, $taxonomy ) {
	$primary_column_content = kenton_beshore_get_extra_theme_option( $tag->slug . '-description-column-1' );
	$primary_column_content = stripslashes($primary_column_content);
	$secondary_column_content = kenton_beshore_get_extra_theme_option( $tag->slug . '-description-column-2' );
	$secondary_column_content = stripslashes($secondary_column_content); ?>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="description-column-1">Primary Column Rich Description</label></th>
		<td><?php wp_editor( $primary_column_content, 'description-column-1' ) ?><br />
		<span class="description-column-1"></span></td>
	</tr>
	<?php if ( $taxonomy == 'media-series' ) : ?>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="description-column-2">Secondary Column Rich Description</label></th>
		<td><?php wp_editor( $secondary_column_content, 'description-column-2' ) ?><br />
		<span class="description-column-2"></span></td>
	</tr>
	<?php endif;
}

add_action( 'edit_term', 'kenton_beshore_save_term_extra_form_fields', 10, 3 );

function kenton_beshore_save_term_extra_form_fields( $term_id, $tt_id, $taxonomy ) {
	if ( $taxonomy != 'media-series' && $taxonomy != 'media-category' )
		return;
	if ( ! isset( $_REQUEST['description-column-1'] ) || ! $_REQUEST['description-column-1'] )
		return;

	$term = get_term( $term_id, $taxonomy );
	kenton_beshore_set_extra_theme_option( $term->slug . '-description-column-1', $_REQUEST['description-column-1'] );
	kenton_beshore_set_extra_theme_option( $term->slug . '-description-column-2', $_REQUEST['description-column-2'] );
}

function kenton_beshore_the_term_primary_description() {
	$queried_object = get_queried_object();
	kenton_beshore_output_term_primary_description( $queried_object->slug );
}

function kenton_beshore_the_term_secondary_description() {
	$queried_object = get_queried_object();
	$secondary_column_content = kenton_beshore_get_extra_theme_option( $queried_object->slug . '-description-column-2' );
	$secondary_column_content = stripslashes( $secondary_column_content );
	$secondary_column_content = do_shortcode( $secondary_column_content );
	echo $secondary_column_content;
}

function kenton_beshore_output_term_primary_description( $term_slug ) {
	$primary_column_content = kenton_beshore_get_extra_theme_option( $term_slug . '-description-column-1' );
	$primary_column_content = stripslashes( $primary_column_content );
	$primary_column_content = do_shortcode( $primary_column_content );
	echo $primary_column_content;
}

/**
 * Whether the post thumbnail should be displayed for the post in the loop.
 */
function kenton_beshore_show_media_thumbnail() {
	if ( ! has_post_thumbnail() )
		return false;

	// always display thumbs on the main media archive
	if ( is_post_type_archive( 'media' ) )
		return true;

	if ( has_term( 'podcast', 'media-category' ) || has_term( 'video', 'media-category' ) ) {
		if ( is_singular() )
			return true;
		if ( is_tax( 'media-category' ) )
			return true;
		if ( is_tax( 'media-series' ) )
			return false;
	}
	return true;
}

function kenton_beshore_is_devotional() {
	if ( has_term( 'devotional', 'media-category' ) )
		return true;
	else
		return false;
}

function kenton_beshore_is_podcast() {
	if ( has_term( 'podcast', 'media-category' ) )
		return true;
	else
		return false;
}

function kenton_beshore_listen_link() {
	?>
	<a class="button" href="<?php echo kenton_beshore_get_listen_permalink() ?>" onclick="javascript:void(window.open('<?php echo kenton_beshore_get_listen_permalink() ?>','kentonbeshorepopupplayer','toolbar=no,location=no,status=no,directories=no,menubar=no,scrolling=auto,scrollbars=auto,width=769,height=846,resizable=yes')); return false;" alt="Listen to this podcast">Listen</a>
	<?php
}

function kenton_beshore_get_the_media_category( $post_id = null ) {
	if ( ! $post_id )
		$post_id = get_the_ID();

	$terms = wp_get_object_terms( $post_id, array( 'media-category' ) );
	if ( ! empty( $terms ) )
		return $terms[0];
	else
		return false;
}


function kenton_beshore_the_media_category() {
	if ( $term = kenton_beshore_get_the_media_category() ) : ?>
	<div class="media-type">
		<a href="<?php echo get_term_link( $term, 'media-category' ) ?>">
			<?php kenton_beshore_the_media_category_glyph() ?>
		</a>
	</div>
	<?php
	endif;
}

function kenton_beshore_the_media_category_glyph( $term = null ) {
	if ( ! $term )
		$term = kenton_beshore_get_the_media_category();
	if ( $term->term_id == 7 ) // video
		echo kenton_beshore_get_glyph( 'video' );
	elseif ( $term->term_id == 4 ) // devotional
		echo kenton_beshore_get_glyph( 'devotional' );
	elseif ( $term->term_id == 3 ) // podcast
		echo kenton_beshore_get_glyph( 'podcast' );
}

function kenton_beshore_get_glyph( $glyph ) {
	switch ( $glyph ) {
		case 'facebook':
			return '&#x4a;';
		case 'video':
			return '&#x51;';
		case 'devotional':
			return '&#x4e;';
		case 'podcast':
			return '&#x4f;';
		case 'itunes':
			return '&#x58;';
		case 'rss':
			return '&#x44;';
		case 'stitcher':
			return '&#x45;';
		case 'right-arrow-big':
			return '&#x49;';
		case 'phone':
			return '&#x4b;';
		case 'twitter':
			return '&#x4d;';
		case 'address':
			return '&#x50;';
		case 'vimeo':
			return '&#x53;';
		case 'youtube':
			return '&#x54;';
		case 'right-arrow':
			return '&#x55;';
		case 'left-arrow':
			return '&#x56;';
		case 'media':
			return '&#x52;';
	}
}

add_action( 'wp_head', 'kenton_beshore_typekit_script' );

function kenton_beshore_typekit_script() {
	?>
	<script type="text/javascript" src="//use.typekit.net/lxz1kun.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<?php
}

add_action( 'init', 'kenton_beshore_remove_addthis_hooked_functions', 15 );
/**
 * Addthis by default just throws the social media widget onto the end of
 * post content. We need more control to output it exactly where we want in our
 * template, so we're going to unhook their default callback.
 */
function kenton_beshore_remove_addthis_hooked_functions() {
	remove_filter( 'wp_head', 'addthis_add_content_filters' );
}

function kenton_beshore_media_description() {
	$description = kenton_beshore_get_theme_option( 'media_archive_header' );
	$description = stripslashes( $description );
	$description = do_shortcode( $description );
	echo $description;
}

add_action( 'post_class', 'kenton_beshore_add_media_category_to_post_class', 10, 3 );
function kenton_beshore_add_media_category_to_post_class( $classes, $class, $post_ID ) {
	$post = get_post( $post_ID );
	if ( 'media' != $post->post_type )
		return;
	$media_category = kenton_beshore_get_the_media_category();
	if ( empty( $media_category ) )
		return;
	$classes[] = 'media-category-' . $media_category->term_taxonomy_id;
	return $classes;
}

remove_all_actions( 'do_feed_rss2' );
add_action( 'do_feed_rss2', 'kenton_beshore_feed_rss2', 10, 1 );
function kenton_beshore_feed_rss2( $for_comments ) {
	$rss_template = get_template_directory() . '/feeds/feed-media-rss2.php';
	if( get_query_var( 'post_type' ) == 'media' || get_query_var( 'media-category' ) )
		load_template( $rss_template );
	else
		do_feed_rss2( $for_comments ); // Call default function
}

function kenton_beshore_rss_content() {
	kenton_beshore_subtitle();
	if ( has_post_thumbnail() )
		the_post_thumbnail( 'large' );
	echo get_the_content_feed('rss2');
	if ( $outline_link = get_post_meta( get_the_ID(), '_media_outline_link', true ) ) { ?>
		<p>Download <a target="_blank" href="<?php echo $outline_link; ?>">Message Outlines</a></p>
	<?php } ?>
	<?php if ( $transcribe_link = get_post_meta( get_the_ID(), '_media_transcribe_link', true ) ) { ?>
		<p>Download <a target="_blank" href="<?php echo $transcribe_link; ?>">Message Transcribe</a></p>
	<?php } ?>
	<?php if ( $flow_questions_link = get_post_meta( get_the_ID(), '_media_flow_questions_link', true ) ) { ?>
		<p>Download <a target="_blank" href="<?php echo $flow_questions_link; ?>">Flow Questions</a></p>
	<?php }
	kenton_beshore_the_media_category_text();
}

function kenton_beshore_the_media_category_text() {
	$categories = wp_get_object_terms( get_the_ID(), 'media-category' );
	foreach ( $categories as $category ) {
		$thelist[] = '<a href="' . esc_url( get_term_link( (int)$category->term_id, 'media-category' ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">' . sprintf( __( "View all %s posts" ), $category->name ) . '</a>';
	}
	if ( ! empty( $thelist ) )
		echo implode( ' ', $thelist );
}

