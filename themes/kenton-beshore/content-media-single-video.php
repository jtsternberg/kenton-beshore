<?php
/**
 * @package kenton-beshore
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="video">
		<?php kenton_beshore_output_post_video(); ?>
	</div>
	<header class="entry-header">
		<?php kenton_beshore_the_media_category() ?>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php kenton_beshore_subtitle() ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="download-links">
			<?php if ( $outline_link = get_post_meta( get_the_ID(), '_media_outline_link', true ) ) { ?>
				<p>Download <a target="_blank" href="<?php echo $outline_link; ?>">Message Outline</a>
			<?php } ?>
			<?php if ( $transcribe_link = get_post_meta( get_the_ID(), '_media_transcribe_link', true ) ) { ?>
				<p>Download <a target="_blank" href="<?php echo $transcribe_link; ?>">Message Transcribe</a></p>
			<?php } ?>
			<?php if ( $flow_questions_link = get_post_meta( get_the_ID(), '_media_flow_questions_link', true ) ) { ?>
				<p>Download <a target="_blank" href="<?php echo $flow_questions_link; ?>">Flow Questions</a></p>
			<?php } ?>
		</div>
		<div class="subscribe">
			<h2>SUBSCRIBE</h2>
			<a target="_blank" alt="Youtube Subscribe Link" class="subscribe-link youtube-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_youtube') ?>">
				<?php echo kenton_beshore_get_glyph( 'youtube' ); ?>
			</a>
			<a target="_blank" alt="Vimeo Subscribe Link" class="subscribe-link vimeo-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_vimeo') ?>">
				<?php echo kenton_beshore_get_glyph( 'vimeo' ); ?>
			</a>
		</div>
		<div class="message-summary">
			<h2>MESSAGE SUMMARY</h2>
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'kenton-beshore' ) ); ?>
		</div>
		<div class="email-this-podcast">
			<h2>EMAIL ME THE LATEST VIDEOS</h2>
			<?php gravity_form( 13, false, false, false, null, false, 10 ); ?>
		</div>
		<div class="share-this-podcast">
			<h2>SHARE THIS VIDEO</h2>
			<?php echo addthis_display_social_widget(''); ?>
		</div>
	</div><!-- .entry-content -->
	<!-- .entry-meta -->
</article><!-- #post-## -->
