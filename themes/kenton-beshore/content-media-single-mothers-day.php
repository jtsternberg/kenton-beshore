<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( kenton_beshore_show_media_thumbnail() ) : ?>
    <div class="video">
		<?php kenton_beshore_output_post_mothers-day(); ?>
	</div>
	<div class="featured-image">
		<a href="<?php the_permalink() ?>">
			<img src="<?php echo kenton_beshore_get_attachment_image_url() ?>" />
		</a>
	</div>
	<?php endif; ?>
	<header class="entry-header">
		<?php kenton_beshore_the_media_category() ?>
		
		<?php kenton_beshore_subtitle() ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
	
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'kenton-beshore' ) ); ?>
		<br />

<p style="text-align: center;">Share this with an amazing women in your life.</p>
		<div class="share-this-podcast2">
			<?php echo addthis_display_social_widget(''); ?>
		</div>
     <!-- .entry-content -->
	<!-- .entry-meta -->
</article><!-- #post-## -->

