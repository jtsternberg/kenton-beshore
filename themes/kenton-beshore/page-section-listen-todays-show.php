<?php
/**
 * Listen | Today's Show template
 */
?>
<?php setup_latest_media_global() ?>
<div class="section-content">
	<h2 style="text-align: center;">The Live Boldly Podcast</h2>
	<div class="section-primary">
		<?php $listen_query = kenton_beshore_listen_todays_show_query(); ?>
		<h2 class="media-title"><?php echo get_the_title( get_latest_media_global_id() ); ?></h2>
		<?php kenton_beshore_from_series() ?>
		<div class="media-excerpt"><p><?php echo kenton_beshore_get_the_excerpt( get_latest_media_global_id() ); ?></p></div>
		<?php echo kenton_beshore_listen_link() ?>
		<div class="subscribe-link-group">
			<p>Subscribe via</p>
			<a target="_blank" alt="iTunes Subscribe Link" class="subscribe-link itunes-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_itunes') ?>"><?php echo kenton_beshore_get_glyph( 'itunes' ); ?></a>
			<a target="_blank" alt="Stitcher Subscribe Link" class="subscribe-link stitcher-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_stitcher') ?>"><?php echo kenton_beshore_get_glyph( 'stitcher' ); ?></a>
			<a target="_blank" alt="RSS Subscribe Link" class="subscribe-link rss-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_rss') ?>"><?php echo kenton_beshore_get_glyph( 'rss' ); ?></a>
		</div>
	</div>
	<div class="section-secondary">
		<div class="media-thumbnail"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/live-boldly-podcast-logo.jpg' ?>" width="300" height="300" /></div>
	</div>
</div><!-- #section-content -->