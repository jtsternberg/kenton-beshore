<?php
/**
 * The Template for displaying all single posts.
 *
 * @package kenton-beshore
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php
			$media_category = kenton_beshore_get_the_media_category();
			switch( $media_category->term_id ) {
				case 3: // podcast
					get_template_part( 'content', 'media-single-podcast' );
					break;
				case 7: // video
					get_template_part( 'content', 'media-single-video' );
					break;
				case 4: // devotional
					get_template_part( 'content', 'media-single-devotional' );
					break;
				default:
					get_template_part( 'content', 'media' );
					break;
				case 26: // lent prayers
					get_template_part( 'content', 'media-single-lent' );
					break;
				case 33: // mothers day post
					get_template_part( 'content', 'media-single-mothers-day' );
					break;
				case 30: // sermonclips
					get_template_part( 'content', 'media-single-sermonclips' );
					break;
			} ?>
			

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>