<?php
/**
 * The Media template file.
 *
 * This template file is theme agnostic.
 *
 */

$outline_link = get_post_meta( get_the_ID(), '_media_outline_link', true );
$audio_link = get_post_meta( get_the_ID(), '_media_audio_link', true );

function get_file_extension_from_url( $url ) {
	$bits = explode( '.', $url );
	return $bits[ count($bits) - 1 ];
}
get_header();
 ?>
	<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="primary">
					<h2><?php the_title(); ?></h2>
					<?php if ( has_media_series() ) : ?>
					<p class="media-series">From <?php the_media_series() ?></p>
					<?php endif; ?>
					<div class="download-links">
						<?php if ( $audio_link = get_post_meta( get_the_ID(), '_media_audio_link', true ) ) { ?>
							<?php echo wp_audio_shortcode( array(
								'src' => $audio_link,
								'preload' => true
							) ); ?>
						<?php } ?>
						<?php if ( $outline_link ) { ?>
							<p>Download <a target="_blank" href="<?php echo $outline_link; ?>">Message Outline</a>
						<?php } ?>
						<?php if ( $transcribe_link = get_post_meta( get_the_ID(), '_media_transcribe_link', true ) ) { ?>
							<p>Download <a target="_blank" href="<?php echo $transcribe_link; ?>">Message Transcribe</a></p>
						<?php } ?>
						<?php if ( $flow_questions_link = get_post_meta( get_the_ID(), '_media_flow_questions_link', true ) ) { ?>
							<p>Download <a target="_blank" href="<?php echo $flow_questions_link; ?>">Flow Questions</a></p>
						<?php } ?>
						<?php if ( $subscribe_to_podcast_link = get_post_meta( get_the_ID(), '_media_subscribe_to_podcast_link', true ) ) { ?>
							<p><a target="_blank" href="<?php echo $subscribe_to_podcast_link; ?>">Subscribe to Podcast Link</a></p>
						<?php } ?>
						<?php if ( $buy_message_link = get_post_meta( get_the_ID(), '_media_buy_message_link', true ) ) { ?>
							<p><a target="_blank" href="<?php echo $buy_message_link; ?>">Buy Message Link</a></p>
						<?php } ?>
						<?php if ( $request_our_current_offer_link = get_post_meta( get_the_ID(), '_media_request_our_current_offer_link', true ) ) { ?>
							<p><a target="_blank" href="<?php echo $request_our_current_offer_link; ?>">Request our current offer link</a></p>
						<?php } ?>
					</div>
					<div class="subscribe-link-group">
						<p>Subscribe via</p>
						<a target="_blank" alt="iTunes Subscribe Link" class="subscribe-link itunes-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_itunes') ?>"><?php echo kenton_beshore_get_glyph( 'itunes' ) ?></a>
						<a target="_blank" alt="Stitcher Subscribe Link" class="subscribe-link stitcher-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_stitcher') ?>"><?php echo kenton_beshore_get_glyph( 'stitcher' ) ?></a>
						<a target="_blank" alt="RSS Subscribe Link" class="subscribe-link rss-subscribe-link" href="<?php echo kenton_beshore_get_theme_option('subscribe_link_rss') ?>"><?php echo kenton_beshore_get_glyph( 'rss' ) ?></a>
					</div>
					<div class="share-this-podcast">
						<h2>SHARE THIS PODCAST</h2>
						<?php echo addthis_display_social_widget(''); ?>
					</div>
				</div><!-- .primary -->
				<div class="secondary">
					<div class="media-thumbnail"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/live-boldly-podcast-logo.jpg' ?>" /></div>
				</div>
			<?php endwhile; ?>

		<?php endif; // end have_posts() check ?>

	</main><!-- .site-main -->
<?php get_footer(); ?>