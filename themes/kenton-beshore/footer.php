<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package kenton-beshore
 */
?>
	<?php if ( kenton_beshore_show_footer() ) : ?>
	<footer id="colophon" class="site-footer section-color-palette--<?php echo kenton_beshore_get_footer_class() ?>" role="contentinfo">
		<div class="container">
			<div class="primary">
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/kenton-beshore-logo-white.png' ?>" alt="Kenton Beshore logo"/>
			</div><!-- .primary -->
			<div class="secondary">
				<div class="contact-info">
					<div class="address"><?php echo kenton_beshore_get_theme_option('address'); ?></div>
					<div class="phone-number"><?php echo kenton_beshore_get_theme_option('phone'); ?></div>
				</div>
				<div class="social-media-links">
					<a target="_blank" class="twitter-link" href="http://twitter.com/<?php echo kenton_beshore_get_theme_option('twitter_handle'); ?>" alt="Kenton Beshore Twitter"><?php echo kenton_beshore_get_glyph( 'twitter' ) ?></a>
					<a target="_blank" class="facebook-link" href="<?php echo kenton_beshore_get_theme_option('facebook_link'); ?>" alt="Kenton Beshore Facebook"><?php echo kenton_beshore_get_glyph( 'facebook' ) ?></a>
				</div>
			</div><!-- .secondary -->
		</div><!-- .container -->
	</footer><!-- #colophon -->
	<?php endif; ?>
	<div class="back-to-top"></div>
</div><!-- #page -->

<script type='text/javascript'>
/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
( function() {
	var container, button, menu;

	container = document.getElementById( 'site-navigation' );
	if ( ! container )
		return;

	button = container.getElementsByTagName( 'h1' )[0];
	if ( 'undefined' === typeof button )
		return;

	menu = container.getElementsByTagName( 'ul' )[0];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	if ( -1 === menu.className.indexOf( 'nav-menu' ) )
		menu.className += ' nav-menu';

	button.onclick = function() {
		if ( -1 !== container.className.indexOf( 'toggled' ) )
			container.className = container.className.replace( ' toggled', '' );
		else
			container.className += ' toggled';
	};
} )();

( function() {
	var is_webkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
	    is_opera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
	    is_ie     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

	if ( ( is_webkit || is_opera || is_ie ) && 'undefined' !== typeof( document.getElementById ) ) {
		var eventMethod = ( window.addEventListener ) ? 'addEventListener' : 'attachEvent';
		window[ eventMethod ]( 'hashchange', function() {
			var element = document.getElementById( location.hash.substring( 1 ) );

			if ( element ) {
				if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) )
					element.tabIndex = -1;

				element.focus();
			}
		}, false );
	}
})();
</script>
<?php wp_footer(); ?>
</body>
</html>