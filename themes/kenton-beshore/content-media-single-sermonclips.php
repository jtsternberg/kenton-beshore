<<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( kenton_beshore_show_media_thumbnail() ) : ?>
	
	<?php endif; ?>
	<header class="entry-header">
		<?php kenton_beshore_the_media_category() ?>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php kenton_beshore_subtitle() ?>
		
	</header><!-- .entry-header -->
	<div class="entry-content">
	
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'kenton-beshore' ) ); ?>
		<br />


		<div class="share-this-podcast2">
			<?php echo addthis_display_social_widget(''); ?>
		</div>
		
		<div class="email-this-podcast3">
			<h2>EMAIL ME THE LATEST #sermonclips</h2>
			<?php gravity_form( 17, false, false, false, null, false, 10 ); ?>
		</div>
		
     <!-- .entry-content -->
	<!-- .entry-meta -->
</article><!-- #post-## -->

