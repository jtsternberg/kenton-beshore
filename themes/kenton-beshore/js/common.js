jQuery(document).ready( function() {
	window.kenton_beshore = {

		/**
		 * Initialize object.
		 */
		init: function() {
			this.resize();
			jQuery(window).resize( this.resize );
			this.initScrollspy();
			this.fixSingleSectionPageHeight();
			jQuery('.back-to-top').click( this.scroll_back_to_top );
			jQuery('.main-navigation .page--contact a').click( kenton_beshore.scroll_to_contact );
			if ( window.location.hash == '#contact' )
				kenton_beshore.scroll_to_contact();
			if ( window.location.hash == '#share-your-story' )
				kenton_beshore.scroll_to_section( '#section--share-this-ministry' );
		},

		/**
		 * Resize handler for the shortcomings of CSS
		 */
		resize: function() {
			kenton_beshore.replace_header_logo_src();
			kenton_beshore.resize_youtube_iframe();
			kenton_beshore.fixSingleSectionPageHeight();
			kenton_beshore.textfill();
			kenton_beshore.resize_reveal();
		},

		/**
		 * Set the Youtube video to 16:9
		 */
		resize_youtube_iframe: function() {
			var iframe_height = jQuery('.section-layout--watch-kentons-latest iframe').width();
			jQuery('.section-layout--watch-kentons-latest iframe').height( iframe_height * 0.5625 );
			iframe_height = jQuery('.section-layout--two-column-content-left iframe').width();
			jQuery('.section-layout--two-column-content-left iframe').height( iframe_height * 0.5625 );
		},

		replace_header_logo_src: function() {
			if (!Modernizr.svg)
				file_extension = '.png';
			else
				file_extension = '.svg';

			if ( common_data.use_blue_logo )
				image_suffix = 'dark-blue' + file_extension;
			else
				image_suffix = 'white' + file_extension;

			jQuery('footer .primary img').attr('src', common_data.theme_url+'/assets/images/kenton-beshore-logo-white' + file_extension );

			if ( jQuery(window).width() < 600 ) {
				jQuery('.site-logo img').attr('src', common_data.theme_url+'/assets/images/kenton-beshore-logo-header-mobile-' + image_suffix );
			} else {
				jQuery('.site-logo img').attr('src', common_data.theme_url+'/assets/images/kenton-beshore-logo-' + image_suffix );
				// old jQuery('.site-logo img, footer .primary img').attr('src', common_data.theme_url+'/assets/images/kenton-beshore-logo-' + image_suffix );
			}
		},

		fixSingleSectionPageHeight: function() {
			$section = jQuery('.section');
			if ( $section.length !== 1 )
				return;
			if ( $section.height() < jQuery(window).height() ) {
				$section.height( jQuery(window).height() );
				$section.css( 'padding-bottom', 0 );
			}
		},
		textfill: function() {
			jQuery('p.section-headline').textfill({
				explicitHeight: 300,
				maxFontPixels: 300
			});
		},
		initScrollspy: function() {
			jQuery('#page').scrollspy( {
				min: jQuery(window).height(),
				max: 100000,
				onEnter: function( element, position ) {
					kenton_beshore.show_back_to_top_button();
				},
				onLeave: function( element, position) {
					kenton_beshore.hide_back_to_top_button();
				}
			} );
		},
		show_back_to_top_button: function() {
			jQuery('.back-to-top').show();
		},
		hide_back_to_top_button: function() {
			jQuery('.back-to-top').hide();
		},
		scroll_back_to_top: function() {
			jQuery('html, body').animate(
				{ scrollTop: 0 },
				2500
			);
		},

		scroll_to_contact: function() {
			kenton_beshore.scroll_to_section( '#section--contact' );
		},
		scroll_to_section: function( section_selector ) {
			$section = jQuery( section_selector );
			if ( $section.length === 0 )
				return;

			jQuery('html, body').animate(
				{ scrollTop: $section.offset().top },
				2500
			);
		},
		resize_reveal: function() {
			if ( jQuery(window).innerWidth() > 1200 ) {
				new_left = ( jQuery(window).innerWidth() - 1200 );
				jQuery('#kentons-list-modal').css( 'left', new_left );
				// jQuery('#kentons-list-modal').css( 'margin-left', '0' );
			} else {
				jQuery('#kentons-list-modal').css( 'left', '45%' );
				// jQuery('#kentons-list-modal').css( 'left', left );

			}

		}

	};

	kenton_beshore.init();

});