<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( kenton_beshore_show_media_thumbnail() ) : ?>
	<div class="featured-image">
		<a href="<?php the_permalink() ?>">
			<img src="<?php echo kenton_beshore_get_attachment_image_url() ?>" />
		</a>
	</div>
	<?php endif; ?>
	<header class="entry-header">
		<?php kenton_beshore_the_media_category() ?>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php kenton_beshore_subtitle() ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<div class="message-summary">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'kenton-beshore' ) ); ?>
		</div><br />
<div id="tell-us"><br />
<p>Share this prayer!</p>
</div>
		<div class="share-this-podcast">
			<?php echo addthis_display_social_widget(''); ?>
		</div>
        <div id="lent">
		Why <a href="http://kenton.org/media/journey-cross/">lent prayers?</a>
        Get the lent prayers <a href="https://my.leadpages.net/leadbox/14ede2973f72a2%3A142a0e046b46dc/5797971773882368/" target="_blank">via email. </a><script type="text/javascript" src="//my.leadpages.net/leadbox-682.js"></script>
		Previous<a href="http://kenton.org/media-category/lent-prayers/"> prayers </a>
	</div><!-- .entry-content -->
	<!-- .entry-meta -->
</article><!-- #post-## -->








