<div class="section-header">Today</div>
<div class="section-content">	
	<div class="section-primary">
		<div class="iphone-outer">
			<div class="iphone-inner">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<div class="section-secondary">
		<a href="<?php echo site_url('/start-here/' ) ?>" class="button">WATCH VIDEO</a>
	</div>
</div><!-- #section-content -->