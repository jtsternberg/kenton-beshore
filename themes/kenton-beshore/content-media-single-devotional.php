<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( kenton_beshore_show_media_thumbnail() ) : ?>
	<div class="featured-image">
		<a href="<?php the_permalink() ?>">
			<img src="<?php echo kenton_beshore_get_attachment_image_url() ?>" />
		</a>
	</div>
	<?php endif; ?>
	<header class="entry-header">
		<?php kenton_beshore_the_media_category() ?>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php kenton_beshore_subtitle() ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<div class="message-summary">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'kenton-beshore' ) ); ?>
		</div>
		<div class="email-this-podcast">
			<h2>EMAIL ME THE LATEST DEVOTIONAL</h2>
			<?php gravity_form( 15, false, false, false, null, false, 10 ); ?>
		</div>
		<div class="share-this-podcast">
			<h2>SHARE THIS DEVOTIONAL</h2>
			<?php echo addthis_display_social_widget(''); ?>
		</div>
	</div><!-- .entry-content -->
	<!-- .entry-meta -->
</article><!-- #post-## -->