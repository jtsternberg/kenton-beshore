<div class="section-content">
	<div class="section-primary">
		<?php the_content(); ?>
	</div>
	<div class="section-secondary">
		<?php kenton_beshore_the_content_second_column(); ?>
	</div>
</div><!-- #section-content -->