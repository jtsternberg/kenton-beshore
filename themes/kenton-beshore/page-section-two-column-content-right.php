<div class="section-content">
	<div class="section-primary">
		<?php the_content(); ?>
	</div>
	<div class="section-secondary">
		<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/arrow-right.png' ?>" width="131" height="134" alt="" />
	</div>
</div><!-- #section-content -->