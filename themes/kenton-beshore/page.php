<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package kenton-beshore
 */

get_header(); ?>

	<main id="main" class="site-main" role="main">

		<?php while ( have_page_sections() ) : the_page_section(); ?>
			<?php get_template_part( 'content', 'page-section' ); ?>
		<?php endwhile; ?>

	</main><!-- #main -->

<?php get_footer(); ?>
