<?php
/**
 * @package kenton-beshore
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( kenton_beshore_show_media_thumbnail() ) : ?>
	<div class="featured-image">
		<a href="<?php the_permalink() ?>">
			<img src="<?php echo kenton_beshore_get_attachment_image_url() ?>" />
		</a>
	</div>
	<?php endif; ?>
	<header class="entry-header">
		<?php kenton_beshore_the_media_category() ?>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php kenton_beshore_subtitle() ?>
	</header><!-- .entry-header -->

	<?php if ( ! is_singular() ) : // Only display Excerpts for non-single pages ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'kenton-beshore' ) ); ?>
		<?php if ( kenton_beshore_is_podcast() ) : ?>
			<a class="button" href="javascript:void(window.open('<?php echo kenton_beshore_get_listen_permalink() ?>','kentonbeshorepopupplayer','toolbar=no,location=no,status=no,directories=no,menubar=no,scrolling=auto,scrollbars=auto,width=640,height=600,resizable=yes')); return false;" alt="Listen to <?php echo esc_attr( get_the_title() ) ?>">Listen</a>
		<?php endif; ?>
	</div><!-- .entry-content -->
	<?php endif; ?>
</article><!-- #post-## -->
<?php if ( ! is_tax( 'media-series' ) ) : ?>
	<hr />
<?php endif;?>