<?php

/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {
	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once get_template_directory() . '/inc/Custom-Metaboxes-and-Fields-for-WordPress/init.php';;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );

add_filter( 'cmb_meta_boxes', 'kenton_beshore_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function kenton_beshore_metaboxes( array $meta_boxes ) {

	$prefix = '_kenton_beshore_';

	$meta_boxes[] = array(
		'id'         => 'page_setion_details_metabox',
		'title'      => 'Page Section Details',
		'pages'      => array( 'page-section', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Layout / Type',
				'desc' => '',
				'id'   => $prefix . 'section_layout_type',
				'type' => 'section_layout_type',
			),
			array(
				'name' => 'Color Palette',
				'desc' => '',
				'id'   => $prefix . 'color_palette',
				'type' => 'color_palette',
			),
			array(
				'name' => 'Second Column Content',
				'desc' => '',
				'id'   => $prefix . 'second_column',
				'type' => 'wysiwyg',
			),
			
		),
	);

	$meta_boxes[] = array(
		'id'         => 'page_details_metabox',
		'title'      => 'Page Details',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Show Footer',
				'desc' => 'Whether to display a footer on this page.',
				'id'   => $prefix . 'show_footer',
				'type' => 'checkbox',
			),
			array(
				'name' => 'Footer Color Palette',
				'desc' => '',
				'id'   => $prefix . 'footer_color_palette',
				'type' => 'color_palette',
			),
			array(
				'name' => 'Temporary page section selector',
				'desc' => 'comma separated page section IDs (e.g. 1,3,5,6)',
				'id'   => $prefix . 'page_sections',
				'type' => 'text',
			),
		),
	);



	return $meta_boxes;
}