// Bind layout highlighter behaviour
jQuery('.cmb-type-color_palette').on(
	'change.kenton_beshore_color_palette_selector',
	'input[type="radio"]',
	kenton_beshore_page_color_palette_highlighter
);

function kenton_beshore_page_color_palette_highlighter(event) {
	'use strict';
	// Cache class name
	var selected_class = 'selected';

    // Remove class from all labels
    jQuery('input[name="' + jQuery(event.target).attr('name') + '"]').parent('label').removeClass(selected_class);

    // Add class to selected layout
    jQuery(event.target).parent('label').addClass(selected_class);
}