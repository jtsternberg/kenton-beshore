<?php
/**
 * Page Section Builder
 *
 */

/**
 * Backbone.js-powered page sections builder.
 */
require get_template_directory() . '/inc/page-sections/page-sections-builder/page-sections-builder.php';


/**
 * Remove the editor from the Page post type.
 *
 * We use Page Sections to build page content instead.
 */
function kenton_beshore_remove_page_editor() {
	remove_post_type_support( 'page', 'editor' );
}
add_action( 'init', 'kenton_beshore_remove_page_editor' );

/**
 * Register the page section post type.
 *
 * Page section post types will be related to pages, and make up the content
 * of a page.
 */
function kenton_beshore_register_page_section_post_type() {
	register_post_type( 'page-section', array(
		'labels' => array(
			'name' => 'Page Sections',
			'singular_name' => 'Page Section',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Page Section',
			'edit_item' => 'Edit Page Section',
			'new_item' => 'New Page Section',
			'view_item' => 'View Page Section',
			'search_items' => 'Search Page Sections',
			'not_found' => 'No page sections found.',
			'not_found_in_trash' => 'No page sections found in Trash.',
			'all_items' => 'All Page Sections'
		),
		'public' => false,
		'publicly_queryable' => false,
		'capability_type' => 'page',
		'show_ui' => true,
		'show_in_menu' => 'edit.php?post_type=page',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => false,
		'query_var' => false,
		'supports' => array( 'title', 'editor', 'revisions' ),
	) );
}
add_action( 'init', 'kenton_beshore_register_page_section_post_type' );

/**
 * Registers new page section layouts.
 */
function kenton_beshore_register_section_layout( $id = '', $args = array() ) {
	global $kenton_beshore;

	if ( empty( $kenton_beshore->section_layouts ) )
		$kenton_beshore->section_layouts = array();

	/** Don't allow empty $id, or double registrations */
	if ( ! $id || isset( $kenton_beshore->section_layouts[$id] ) )
		return false;

	$defaults = array(
		'label' => __( 'No Label Selected', 'kenton-beshore' ),
		'img'   => get_template_directory_uri() . '/inc/page-sections/layouts/none.gif',
		'type'  => 'site',
	);

	$args = wp_parse_args( $args, $defaults );

	$kenton_beshore->section_layouts[$id] = $args;

	return $args;

}


add_action( 'init', 'kenton_beshore_create_initial_layouts', 0 );
/**
 * Registers default section layouts.
 */
function kenton_beshore_create_initial_layouts() {

	/** Common path to default layout images */
	$url = get_template_directory_uri() . '/inc/page-sections/layouts/';


	kenton_beshore_register_section_layout(
		'two-column-content-right',
		array(
			'label' => __( 'Two Column, Content Right', 'kenton-beshore' ),
			'img'   => $url . 'two-col-content-right.gif',
		)
	);

	kenton_beshore_register_section_layout(
		'two-column-content-left',
		array(
			'label' => __( 'Two Column, Content Left', 'kenton-beshore' ),
			'img'   => $url . 'two-col-content-left.gif',
		)
	);
	
	kenton_beshore_register_section_layout(
		'two-col',
		array(
			'label' => __( 'Two Column', 'kenton-beshore' ),
			'img'   => $url . 'two-col.gif',
		)
	);

	kenton_beshore_register_section_layout(
		'one-column',
		array(
			'label' => __( 'One column', 'kenton-beshore' ),
			'img'   => $url . 'one-col.gif',
		)
	);

	kenton_beshore_register_section_layout(
		'listen-todays-show',
		array(
			'label' => __( 'Listen Today\'s Show', 'kenton-beshore' ),
			'img'   => $url . 'listen-todays-show.gif',
		)
	);

	kenton_beshore_register_section_layout(
		'watch-kentons-latest',
		array(
			'label' => __( 'Watch Kenton\'s Latest', 'kenton-beshore' ),
			'img'   => $url . 'watch-kentons-latest.gif',
		)
	);

	kenton_beshore_register_section_layout(
		'read-kentons-latest',
		array(
			'label' => __( 'Read Kenton\'s Latest', 'kenton-beshore' ),
			'img'   => $url . 'read-kentons-latest.gif',
		)
	);

	kenton_beshore_register_section_layout(
		'contact',
		array(
			'label' => __( 'Contact', 'kenton-beshore' ),
			'img'   => $url . 'contact.gif',
		)
	);

	kenton_beshore_register_section_layout(
		'latest-tweets',
		array(
			'label' => __( 'Latest Tweets', 'kenton-beshore' ),
			'img'   => $url . 'latest-tweets.gif',
		)
	);
	
	kenton_beshore_register_section_layout(
		'today',
		array(
			'label' => __( 'Today', 'kenton-beshore' ),
			'img'   => $url . 'today.gif',
		)
	);
	
	kenton_beshore_register_section_layout(
		'church',
		array(
			'label' => __( 'Church', 'kenton-beshore' ),
			'img'   => $url . 'church.gif',
		)
	);
	
	kenton_beshore_register_section_layout(
		'about',
		array(
			'label' => __( 'About', 'kenton-beshore' ),
			'img'   => $url . 'about.gif',
		)
	);

}


/**
 * Returns all registered section layouts.
 */
function kenton_beshore_get_section_layouts( $type = '' ) {
	global $kenton_beshore;

	/** If no layouts exists, return empty array */
	if ( ! is_array( $kenton_beshore->section_layouts ) ) {
		$kenton_beshore->section_layouts = array();
		return $kenton_beshore->section_layouts;
	}

	/** Return all layouts, if no type specified */
	if ( '' == $type )
		return $kenton_beshore->section_layouts;

	$layouts = array();

	/** Cycle through looking for layouts of $type */
	foreach ( (array) $kenton_beshore->section_layouts as $id => $data ) {
		if ( $data['type'] == $type )
			$layouts[$id] = $data;
	}

	return $layouts;
}

add_action( 'cmb_render_section_layout_type', 'kenton_beshore_cmb_render_section_layout_type' );

/**
 * Helper function that outputs the form elements necessary to select a layout.
 *
 * You must manually wrap this in an HTML element with the class of
 * 'genesis-layout-selector' in order for the CSS and Javascript to apply properly.
 *
 * Supported $args keys are:
 *   name (default is ''),
 *   selected (default is ''),
 *   echo (default is true).
 *
 * The Genesis admin script is enqueued to ensure the layout selector behaviour
 * (amending label class to add border on selected layout) works.
 *
 * @since 1.7.0
 *
 * @uses genesis_get_layouts() Get all registered layouts
 *
 * @param array $args Optional. Function arguments. Default is empty array
 * @return string HTML markup of labels, images and radio inputs for layout selector
 */
function kenton_beshore_cmb_render_section_layout_type() {
	wp_nonce_field( plugin_basename( __FILE__ ), '_kenton_beshore_section_layout' );

	$layout = get_post_meta( get_the_ID(), '_kenton_beshore_section_layout_type', true );

	if ( ! $layout )
		$layout = 'two-column-content-right';

	/** Enqueue the Javascript and CSS */
	wp_enqueue_script(
		'section-layout-type-selector',
		get_template_directory_uri() . '/inc/page-sections/layout-type-selector.js',
		array( 'jquery' ),
		'20130903',
		true
	);

	wp_enqueue_style(
		'section-layout-type-selector',
		get_template_directory_uri() . '/inc/page-sections/layout-type-selector.css'
	);

	/** Merge defaults with user args */
	$args = array(
		'name'     => '_kenton_beshore_section_layout_type',
		'selected' => $layout,
		'type'     => '',
		'echo'     => true,
	);

	$output = '';

	foreach ( kenton_beshore_get_section_layouts( $args['type'] ) as $id => $data ) {
		$class = $id == $args['selected'] ? ' selected' : '';

		$output .= sprintf(
			'<label title="%1$s" class="box%2$s"><img src="%3$s" alt="%1$s" /><br /> <input type="radio" name="%4$s" id="%5$s" value="%5$s" %6$s /></label>',
			esc_attr( $data['label'] ),
			esc_attr( $class ),
			esc_url( $data['img'] ),
			esc_attr( $args['name'] ),
			esc_attr( $id ),
			checked( $id, $args['selected'], false )
		);
	}

	/** Echo or return output */
	if ( $args['echo'] )
		echo $output;
	else
		return $output;
}