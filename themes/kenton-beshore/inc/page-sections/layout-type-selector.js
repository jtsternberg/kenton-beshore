// Bind layout highlighter behaviour
jQuery('.cmb-type-section_layout_type').on(
	'change.kenton_beshore_section_layout_selector',
	'input[type="radio"]',
	kenton_beshore_page_section_layout_highlighter
);

function kenton_beshore_page_section_layout_highlighter(event) {
	'use strict';
	// Cache class name
	var selected_class = 'selected';

	// Remove class from all labels
	jQuery('input[name="' + jQuery(event.target).attr('name') + '"]').parent('label').removeClass(selected_class);

	// Add class to selected layout
	jQuery(event.target).parent('label').addClass(selected_class);
	kenton_beshore_display_fields_for_layout( jQuery(event.target).attr('id') );
}

/**
 * Hide all page section fields.
 */
function kenton_beshore_hide_all_extra_page_section_details() {
	jQuery('.cmb_id__kenton_beshore_second_column').hide();
}

jQuery(document).ready( function() {
	var selected_id = jQuery('input[name="_kenton_beshore_section_layout_type"]:checked').attr( 'id' );
	kenton_beshore_hide_all_extra_page_section_details();
	if ( selected_id )
		kenton_beshore_display_fields_for_layout( selected_id );
	else
		kenton_beshore_hide_all_extra_page_section_details();
});

/**
 * Display fields that pertain to the given layout type.
 * 
 * @param $layout string Layout slug.
 */
function kenton_beshore_display_fields_for_layout( layout ) {
	kenton_beshore_hide_all_extra_page_section_details();
	switch ( layout ) {
		case 'two-column-content-left':
			break;
		case 'contact':

			break;
		case 'two-col':
		case 'about':
			jQuery('.cmb_id__kenton_beshore_second_column').show();
			break;
		default:
			break;
	}
}