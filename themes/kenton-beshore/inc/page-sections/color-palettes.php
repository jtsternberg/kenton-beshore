<?php
/**
 * Color Palettes
 *
 */

/**
 * Registers new page section layouts.
 */
function kenton_beshore_register_color_palette( $id = '', $args = array() ) {
	global $kenton_beshore;

	if ( empty( $kenton_beshore->color_palettes ) )
		$kenton_beshore->color_palettes = array();

	/** Don't allow empty $id, or double registrations */
	if ( ! $id || isset( $kenton_beshore->color_palettes[$id] ) )
		return false;

	$defaults = array(
		'label' => __( 'No Label Selected', 'kenton-beshore' ),
		'img'   => get_template_directory_uri() . '/inc/page-sections/color-palettes/none.gif',
		'type'  => 'site',
	);

	$args = wp_parse_args( $args, $defaults );

	$kenton_beshore->color_palettes[$id] = $args;

	return $args;

}


add_action( 'init', 'kenton_beshore_create_initial_color_palettes', 0 );
/**
 * Registers default section color_palettes.
 */
function kenton_beshore_create_initial_color_palettes() {

	/** Common path to default layout images */
	$url = get_template_directory_uri() . '/inc/page-sections/color-palettes/';


	kenton_beshore_register_color_palette(
		'1',
		array(
			'label' => __( 'White on Dark Blue', 'kenton-beshore' ),
			'img'   => $url . '1.gif',
		)
	);

	kenton_beshore_register_color_palette(
		'2',
		array(
			'label' => __( 'White on Teal', 'kenton-beshore' ),
			'img'   => $url . '2.gif',
		)
	);

	kenton_beshore_register_color_palette(
		'3',
		array(
			'label' => __( 'White on Orange', 'kenton-beshore' ),
			'img'   => $url . '3.gif',
		)
	);

	kenton_beshore_register_color_palette(
		'4',
		array(
			'label' => __( 'Teal on Chick Yellow', 'kenton-beshore' ),
			'img'   => $url . '4.gif',
		)
	);

	kenton_beshore_register_color_palette(
		'5',
		array(
			'label' => __( 'Dark Blue on White', 'kenton-beshore' ),
			'img'   => $url . '5.gif',
		)
	);
	
	kenton_beshore_register_color_palette(
		'6',
		array(
			'label' => __( 'Dark Blue on Lavender', 'kenton-beshore' ),
			'img'   => $url . '6.gif',
		)
	);
	
	kenton_beshore_register_color_palette(
		'7',
		array(
			'label' => __( 'White on Punch Red', 'kenton-beshore' ),
			'img'   => $url . '7.gif',
		)
	);

}


/**
 * Returns all registered section layouts.
 */
function kenton_beshore_get_color_palettes( $type = '' ) {
	global $kenton_beshore;

	/** If no layouts exists, return empty array */
	if ( ! is_array( $kenton_beshore->color_palettes ) ) {
		$kenton_beshore->color_palettes = array();
		return $kenton_beshore->color_palettes;
	}

	/** Return all layouts, if no type specified */
	if ( '' == $type )
		return $kenton_beshore->color_palettes;

	$color_palettes = array();

	/** Cycle through looking for layouts of $type */
	foreach ( (array) $kenton_beshore->color_palettes as $id => $data ) {
		if ( $data['type'] == $type )
			$color_palettes[$id] = $data;
	}

	return $color_palettes;
}

add_action( 'cmb_render_color_palette', 'kenton_beshore_cmb_render_color_palette' );

/**
 * Helper function that outputs the form elements necessary to select a layout.
 *
 * You must manually wrap this in an HTML element with the class of
 * 'genesis-layout-selector' in order for the CSS and Javascript to apply properly.
 *
 * Supported $args keys are:
 *   name (default is ''),
 *   selected (default is ''),
 *   echo (default is true).
 *
 * The Genesis admin script is enqueued to ensure the layout selector behaviour
 * (amending label class to add border on selected layout) works.
 *
 * @since 1.7.0
 *
 * @uses genesis_get_layouts() Get all registered layouts
 *
 * @param array $args Optional. Function arguments. Default is empty array
 * @return string HTML markup of labels, images and radio inputs for layout selector
 */
function kenton_beshore_cmb_render_color_palette() {

	wp_nonce_field( plugin_basename( __FILE__ ), '_kenton_beshore_color_palette_nonce' );

	$color_palette = get_post_meta( get_the_ID(), '_kenton_beshore_color_palette', true );

	if ( ! $color_palette )
		$color_palette = '1';
	/** Enqueue the Javascript and CSS */
	wp_enqueue_script(
		'color-palette-selector',
		get_template_directory_uri() . '/inc/page-sections/color-palette-selector.js',
		array( 'jquery' ),
		'20130903',
		true
	);

	wp_enqueue_style(
		'color-palette-selector',
		get_template_directory_uri() . '/inc/page-sections/color-palette-selector.css'
	);

	/** Merge defaults with user args */
	$args = array(
		'name'     => '_kenton_beshore_color_palette',
		'selected' => $color_palette,
		'type'     => '',
		'echo'     => true,
	);

	$output = '';

	foreach ( kenton_beshore_get_color_palettes( $args['type'] ) as $id => $data ) {
		$class = $id == $args['selected'] ? ' selected' : '';

		$output .= sprintf(
			'<label title="%1$s" class="box%2$s"><img src="%3$s" alt="%1$s" /><br /> <input type="radio" name="%4$s" id="%5$s" value="%5$s" %6$s /></label>',
			esc_attr( $data['label'] ),
			esc_attr( $class ),
			esc_url( $data['img'] ),
			esc_attr( $args['name'] ),
			esc_attr( $id ),
			checked( $id, $args['selected'], false )
		);
	}

	/** Echo or return output */
	if ( $args['echo'] )
		echo $output;
	else
		return $output;
}
