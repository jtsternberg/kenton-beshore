<?php

add_action( 'admin_init', 'kenton_beshore_register_page_section_builder_script' );

/**
 * Register styles and scripts for the page section builder.
 */
function kenton_beshore_register_page_section_builder_script() {
	wp_register_script(
		'bootstrap',
		get_stylesheet_directory_uri() . '/inc/page-sections/page-sections-builder/lib/bootstrap.js'
	);
	wp_register_script(
		'page-sections-builder',
		get_stylesheet_directory_uri() . '/inc/page-sections/page-sections-builder/sections.js',
		array( 'backbone', 'underscore', 'jquery', 'bootstrap' )
	);

	wp_register_style( 'bootstrap', get_stylesheet_directory_uri() . '/inc/page-sections/page-sections-builder/css/bootstrap.min.css' );

	wp_register_style( 'page-sections-builder', get_stylesheet_directory_uri() . '/inc/page-sections/page-sections-builder/css/page-sections-builder.css', array( 'bootstrap' ) );
}
add_action( 'cmb_render_page_section_selector', 'kenton_beshore_cmb_render_page_section_selector' );

/**
 * Helper function that outputs the markup for the page section builder.
 */
function kenton_beshore_cmb_render_page_section_selector() {
	wp_nonce_field( plugin_basename( __FILE__ ), '_kenton_beshore_page_section_selector' );

	$page_sections = get_post_meta( get_the_ID(), '_kenton_beshore_page_section_selector', true );

	if ( ! $page_sections )
		$page_sections = '';

	/** Enqueue the Javascript and CSS */
	wp_enqueue_script( 'page-sections-builder' );

	wp_enqueue_style( 'page-sections-builder' );

	require( 'prototype.php' );
}