<div id="page-sections">
	<ul class="page-sections">
		<li>
			Introduction
			<div class="controls">
				<i class="icon-list control order"></i>
				<i class="icon-remove control remove"></i>
			</div>
		</li>
	</ul>
	<div class="btn-group control add">
		<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
			Add Section
			<span class="caret"></span>
		</a>
		<ul class="dropdown-menu"></ul>
	</div>
</div>
<script type="text/x-template" id="template-add-item">
	<li>
		<a href="#" data-id="<%= id %>"><%= title %></a>
	</li>
</script>
<script type="text/x-template" id="template-config-item">
	<li>
		<%= title %>
		<div class="controls">
			<i class="icon-list control order"></i>
			<i class="icon-remove control remove"></i>
		</div>
	</li>
</script>
