(function( $, _, Backbone ) {

	$(document).ready(function() {

		var Section = Backbone.Model.extend({
			defaults: {
				title: "",
				order: 0
			}
		});

		var Sections = Backbone.Collection.extend({
			model: Section
		});

		var SectionList = new Sections([
			{ id: 1, title: "Introduction" },
			{ id: 2, title: "Biography" },
			{ id: 3, title: "Features" },
			{ id: 4, title: "Additional Reading" }
		]);

		var PageSections = new Sections();

		var SectionConfig = Backbone.View.extend({
			events: {
				"click .control.add .dropdown-menu a": "addSection"
			},

			initialize: function() {
				this.count = 0;

				this.$config = this.$( '.page-sections' );
				this.$add_list = this.$( '.control.add .dropdown-menu' );

				PageSections.on( 'add remove', this.renderConfig, this );

				this.render();
			},

			render: function() {
				this.$add_list.empty();
				SectionList.each( function( e, i, l ) {
					this.$add_list.append( _.template( $( "#template-add-item" ).text() )( e.attributes ) );
				}, this );

				this.renderConfig();
			},

			renderConfig: function() {
				this.$config.empty();
				PageSections.each( function( e, i, l ) {
					this.$config.append( _.template( $( "#template-config-item" ).text() )( e.attributes ) );
				}, this );
			},

			addSection: function( event ) {
				var $t = $( event.target ), model = SectionList.get( $t.attr( 'data-id' ) );
				var atts = {
					sec_id: model.get( 'id' ),
					title: model.get( 'title' ),
					order: this.count++
				};

				PageSections.add( atts );
				console.log( "Added: ", atts );
			}
		});

		$(function() {
			new SectionConfig({ el: "#page-sections" });
		});
	})
})( jQuery, _, Backbone );