<?php



function kenton_beshore_register_settings_page() {
    $settings_page = new WP_Settings_Page( array(
        'slug' => 'kenton_beshore_theme_settings',
        'menu_title' => 'Theme Settings',
        'page_title' => 'Theme Settings',
        'parent_slug' => 'themes.php'
    ) );

    $settings_page->add_section( array(
        'slug' => 'contact_info',
        'title' => 'Contact Info',
        'settings_page' => 'kenton_beshore_theme_settings'
    ) );

	$settings_page->add_section( array(
        'slug' => 'subscribe_links',
        'title' => 'Subscribe Links',
        'settings_page' => 'kenton_beshore_theme_settings'
    ) );

    $settings_page->add_section( array(
        'slug' => 'podcast_info',
        'title' => 'Podcast Info',
        'settings_page' => 'kenton_beshore_theme_settings'
    ) );

    $settings_page->add_section( array(
        'slug' => 'media_settings',
        'title' => 'Media Item Settings',
        'settings_page' => 'kenton_beshore_theme_settings'
    ) );

    $settings_page->get_section( 'contact_info' )->add_field( array(
        'slug' => 'phone',
        'title' => 'Phone #'
    ) );

    $settings_page->get_section( 'contact_info' )->add_field( array(
        'slug' => 'address',
        'title' => 'Mailing Address'
    ) );

    $settings_page->get_section( 'contact_info' )->add_field( array(
        'slug' => 'twitter_handle',
        'title' => 'Twitter Handle'
    ) );

    $settings_page->get_section( 'contact_info' )->add_field( array(
        'slug' => 'facebook_link',
        'title' => 'Facebook Link'
    ) );

	$settings_page->get_section( 'subscribe_links' )->add_field( array(
        'slug' => 'subscribe_link_itunes',
        'title' => 'iTunes Subscribe Link'
    ) );

	$settings_page->get_section( 'subscribe_links' )->add_field( array(
        'slug' => 'subscribe_link_stitcher',
        'title' => 'Stitcher Subscribe Link'
    ) );

	$settings_page->get_section( 'subscribe_links' )->add_field( array(
        'slug' => 'subscribe_link_rss',
        'title' => 'RSS Subscribe Link'
    ) );

	$settings_page->get_section( 'subscribe_links' )->add_field( array(
        'slug' => 'subscribe_link_vimeo',
        'title' => 'Vimeo Subscribe Link'
    ) );

	$settings_page->get_section( 'subscribe_links' )->add_field( array(
        'slug' => 'subscribe_link_youtube',
        'title' => 'Youtube Subscribe Link'
    ) );

    $settings_page->get_section( 'podcast_info' )->add_field( array(
        'slug' => 'rate_this_podcast_text',
        'title' => 'Rate this podcast text',
        'field_type' => 'textarea'
    ) );

    $settings_page->get_section( 'podcast_info' )->add_field( array(
        'slug' => 'music_for_this_podcast_text',
        'title' => 'Music for this podcast text',
        'field_type' => 'textarea'
    ) );

    $settings_page->get_section( 'media_settings' )->add_field( array(
        'slug' => 'media_archive_header',
        'title' => 'Media Archive Header',
        'field_type' => 'textarea'
    ) );
}

add_action( 'admin_menu', 'kenton_beshore_register_settings_page' );

class WP_Settings_Page {

	/**
	 * Array of WP_Settings_Sections objects
	 */
	var $sections = array();

	function __construct( $args = array() ) {

		$defaults = array(
			'page_title'      => 'Settings Page Title',          // The title to be displayed in the browser window on the settings page
			'menu_title'      => 'Settings Menu Title',           // Menu item text for the settings page
			'capability'      => 'manage_options',               // Which type of users can see this menu item
			'slug'            => '',                             // The unique slug for this menu item
			'render_callback' => array( $this, 'render_page' ),        // The rendering callback
			'parent_slug'     => '',
			'icon_url'        => '',
			'position'        => NULL,
		);

		$args = wp_parse_args( $args, $defaults );

		$this->page_title = $args['page_title'];
		$this->menu_title = $args['menu_title'];
		$this->capability = $args['capability'];
		$this->slug = $args['slug'];
		$this->render_callback = $args['render_callback'];
		$this->parent_slug = $args['parent_slug'];
		$this->icon_url = $args['icon_url'];
		$this->position = $args['position'];

		if ( $this->parent_slug ) {
			add_submenu_page(
				$this->parent_slug,
				$this->page_title,
				$this->menu_title,
				$this->capability,
				$this->slug,
				$this->render_callback
			);
		} else {
			add_menu_page(
				$this->page_title,
				$this->menu_title,
				$this->capability,
				$this->slug,
				$this->render_callback,
				$this->icon_url,
				$this->position
			);
		}

		register_setting( $this->slug, $this->slug );
	}

	/**
	 * Render the settings page
	 */
	function render_page() {
		?>
			<!-- Create a header in the default WordPress 'wrap' container -->
			<div class="wrap">

				<div id="icon-themes" class="icon32"></div>
				<h2><?php echo $this->page_title; ?></h2>
				<?php settings_errors(); ?>

				<form method="post" action="options.php">
					<?php settings_fields( $this->slug ); ?>
					<?php do_settings_sections( $this->slug ); ?>
					<?php submit_button(); ?>
				</form>

			</div><!-- /.wrap -->
		<?php
	}

	/**
	 * Add a settings section to this settings page.
	 *
	 * @see WP_Settings_Section::__construct()
	 */
	function add_section( $args ) {
		$this->sections[$args['slug']] = new WP_Settings_Section( $args );
	}

	function get_section( $slug ) {
		return $this->sections[$slug];
	}

}

class WP_Settings_Section {

	var $fields = array();

	/**
	 * @param array $args {
	 *     An array of arguments. Required.
	 *
	 *     @type string   'slug'            Unique identifying slug.
	 *     @type string   'title'           Title for the header of the section.
	 *     @type string   'settings_page'   Slug for the settings page this section will be shown on
	 *     @type callback 'render_callback' Render callback for the section
	 * }
	 */
	function __construct( $args ) {
		$defaults = array(
			'slug'            => '',
			'title'           => '',
			'settings_page'   => NULL,
			'render_callback' => array( $this, 'render' )
		);

		$args = wp_parse_args( $args, $defaults );

		$this->slug = $args['slug'];
		$this->title = $args['title'];
		$this->settings_page = $args['settings_page'];
		$this->render_callback = $args['render_callback'];

		add_settings_section(
			$this->slug,
			$this->title,
			$this->render_callback,
			$this->settings_page
		);
	}

	/**
	 * Built-in render callback that does nothing.
	 *
	 * Override in a subclass.
	 */
	function render() {}

	/**
	 * Adds a settings field as a child of this section.
	 *
	 * @see WP_Settings_Field::__construct()
	 */
	function add_field( $args = array() ) {
		$args['section'] = $this->slug;
		$args['settings_page'] = $this->settings_page;
		$fields[$args['slug']] = new WP_Settings_Field( $args );
	}
}

class WP_Settings_Field {

	/**
	 * @param array $args {
	 *     An array of arguments. Required.
	 *
	 *     @type string   'slug'            Unique identifying slug.
	 *     @type string   'title'           Title that will be output for the field.
	 *     @type callback 'render_callback' Render callback for the section
	 *     @type string   'settings_page'   Slug of the settings page this section will be shown on
	 *     @type string   'section'   Slug of the section this field will be shown in
	 *     @type string   'field_type'   The type of input field
	 * }
	 */
	function __construct( $args = array() ) {
		$defaults = array(
			'slug'            => '',
			'title'           => '',
			'render_callback' => array( $this, 'render' ),
			'settings_page'   => NULL,
			'section'         => '',
			'field_type'      => 'text'
		);

		$args = wp_parse_args( $args, $defaults );

		$this->slug = $args['slug'];
		$this->title = $args['title'];
		$this->render_callback = $args['render_callback'];
		$this->settings_page = $args['settings_page'];
		$this->section = $args['section'];
		$this->field_type = $args['field_type'];

		// Preload the value of this field
		$option = get_option( $this->settings_page );
		if ( ! empty( $option[$this->slug] ) )
			$this->value = $option[$this->slug];
		else
			$this->value = '';

		add_settings_field(
			$this->slug,
			$this->title,
			$this->render_callback,
			$this->settings_page,
			$this->section
		);

	}


	/**
	 * The main render method.
	 *
	 * Calls a submethod depending on the field_type.
	 */
	function render() {
		$sub_render_method = 'render_' . $this->field_type;
		call_user_func( array( $this, $sub_render_method ) );
	}

	/**
	 * Render a simple text input box
	 */
	function render_text() {
		?><input type="text" id="<?php echo $this->slug ?>" name="<?php echo $this->settings_page ?>[<?php echo $this->slug ?>]" value="<?php echo $this->value ?>" ><?php
	}

	/**
	 * Render a textarea
	 */
	function render_textarea() {
		?><textarea id="<?php echo $this->slug ?>" name="<?php echo $this->settings_page ?>[<?php echo $this->slug ?>]"><?php echo $this->value ?></textarea><?php
	}

}