<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package kenton-beshore
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<p class="section-headline"><span>404</span></p>

				<div class="page-content">
					<p>Holy smokes! Unfortunately, the information you're looking for is unavailable.</p>

					<h2>But wait!</h2>
					<p>DON'T LEAVE EMPTY HANDED</p>
					<p>GRAB THIS FREE REPORT</p>
					
					<a href='#' class='button' data-reveal-id="kentons-list-modal" data-animation="none">Click here for instant access</a>

					<?php kenton_beshore_output_kentons_list_lightbox() ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>