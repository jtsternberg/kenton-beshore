<?php $read_query = kenton_beshore_read_todays_show_query(); ?>
<?php while( $read_query->have_posts() ) : $read_query->the_post(); ?>
<?php if ( has_post_thumbnail() ) : ?>
<div class="featured-image-banner" data-background-ratio="2.0315" style="<?php echo kenton_beshore_output_featured_image_banner_style() ?>"></div>
<?php endif; ?>
<div class="section-content">
	
	<div class="section-primary">
		<h2><?php the_title() ?></h2>
		<?php kenton_beshore_subtitle() ?>
		<?php the_excerpt() ?>
		<a class="button" href="<?php echo get_permalink( get_the_ID() ) ?>">READ MORE</a>
	</div>
</div><!-- #section-content -->
<?php endwhile; ?>
<?php wp_reset_postdata() ?>