<div class="featured-image-banner"></div>
<div class="section-content">
	<h1 class="section-title"><span class="title-primary">ABOUT | </span><span class="title-secondary">KENTON<br />BESHORE</span></h1>
	<div class="section-primary">
		<?php the_content(); ?>
		<p>Subscribe via:</p>
		<div class="social-media-links">
			<a class="twitter-link" href="http://twitter.com/<?php echo kenton_beshore_get_theme_option('twitter_handle'); ?>" alt="Kenton Beshore Twitter">
			</a>
			<a class="facebook-link" href="<?php echo kenton_beshore_get_theme_option('facebook_link'); ?>" alt="Kenton Beshore Facebook">
			</a>
		</div>
	</div>
	<div class="section-secondary">
		<?php kenton_beshore_the_content_second_column(); ?>
	</div>
</div><!-- #section-content -->