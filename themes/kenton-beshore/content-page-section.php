<?php  global $page_sections_query; ?>
	<?php if ( $page_sections_query->current_post === 0 ) :?>
	<div id="contentLayer"></div>
	<?php endif; ?>
<section id="section--<?php echo sanitize_title( get_the_title() ) ?>"
		 class="section
                section--<?php echo $page_sections_query->current_post ?>
                section-layout--<?php echo kenton_beshore_get_section_layout() ?>
                section-color-palette--<?php echo kenton_beshore_get_section_color_palette() ?> ">
	<?php if ( $page_sections_query->current_post === 0 ) :?>
	<?php kenton_beshore_load_menu() ?>
	<?php endif; ?>
	<?php kenton_beshore_load_page_section_template() ?>
</section><!-- .section -->
<?php do_action( 'after_section' );